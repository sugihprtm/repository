<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Prodi extends CI_Controller {

 	public function __construct()
    {
        parent::__construct(); 
        $this->load->model('m_majors');
        $this->auth->restrict();
        $this->auth->admin();
    }

    private static $title = "Program Studi &minus; SISTEM INFORMASI TUGAS AKHIR - FASILKOM UNSIKA";
    private static $table = 'prodi';
	private static $primaryKey = 'm_id';

    public function index()
	{
        $data['title'] = "Data ".self::$title;
        $data['content'] = "dashboard/prodi";
		$this->load->view('dashboard/index', $data);
	}

    public function get_data()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            $this->load->library('datatables_ssp');
            $columns = array(
                array('db' => 'm_id', 'dt' => 'm_id'),
                array('db' => 'prodi', 'dt' => 'prodi'),
                array('db' => 'm_created_at', 'dt' => 'm_created_at'),
                array('db' => 'm_updated_at', 'dt' => 'm_updated_at'),
                array(
                    'db' => 'm_id',
                    'dt' => 'tindakan',
                    'formatter' => function($m_id) {
                        return '<a class="btn btn-info btn-sm mb" href="'.site_url('prodi/edit/'.$m_id).'">Ubah</a>
                        <a class="btn btn-danger btn-sm mb" onclick="return confirmDialog();" href="'.site_url('prodi/delete/'.$m_id).'">Hapus</a>';
                    }
                ),
            );

            $sql_details = [
                'user' => $this->db->username,
                'pass' => $this->db->password,
                'db' => $this->db->database,
                'host' => $this->db->hostname
            ];

            echo json_encode(
                Datatables_ssp::complex($_GET, $sql_details, self::$table, self::$primaryKey, $columns, NULL, "m_is_deleted = 'FALSE'")
            );
        }
    }

    public function add()
    {
        $this->load->helper(['form', 'notification']);

        if ($this->validation()) {

            $m_id = $this->input->post('m_id', TRUE);
            $prodi = $this->input->post('prodi', TRUE);
            $where = "m_id = '$m_id' OR prodi = '$prodi'";

            $data = $this->m_majors->is_exist($where);
            if (strtolower($data['prodi']) === strtolower($this->input->post('prodi', TRUE))) {
                $this->session->set_flashdata('alert', error('Nama Program Studi sudah ada!'));
                $data['title'] = "Tambah ".self::$title;
                $data['form_title'] = "Tambah Program Studi";
                $data['action'] = site_url(uri_string());
                $data['content'] = 'dashboard/prodi-form';
                $this->load->view('dashboard/index', $data);

            } else if (strtolower($data['m_id']) === strtolower($this->input->post('m_id', TRUE))) {
                $this->session->set_flashdata('alert', error('Kode Program Studi sudah ada!'));
                $data['title'] = "Tambah ".self::$title;
                $data['form_title'] = "Tambah Program Studi";
                $data['action'] = site_url(uri_string());
                $data['content'] = 'dashboard/prodi-form';
                $this->load->view('dashboard/index', $data);

            } else {
                $data = [
                    'm_id' => strtoupper($this->input->post('m_id', TRUE)),
                    'prodi' => ucfirst($this->input->post('prodi', TRUE)),
                    'm_created_by' => $this->session->userdata['u_name'],
                    'm_is_deleted' => 'FALSE'
                ];

                $this->m_majors->add($data);
                $this->session->set_flashdata('alert', success('Data Program Studi berhasil ditambahkan.'));
                $data['title'] = "Data ".self::$title;
                $data['content'] = "dashboard/prodi";
                redirect('prodi');
            }

        } else {
            $data['prodi'] = FALSE;
            $data['title'] = "Tambah ".self::$title;
            $data['form_title'] = "Tambah Program Studi";
            $data['action'] = site_url(uri_string());
            $data['content'] = 'dashboard/prodi-form';
            $this->load->view('dashboard/index', $data);
        }
    }

    public function edit()
    {
        $this->load->helper(['form', 'notification']);
        $m_id = $this->uri->segment(3);

        if ($this->validation()) {

            $data = [
                'm_id' => strtoupper($this->input->post('m_id', TRUE)),
                'prodi' => ucfirst($this->input->post('prodi', TRUE)),
                'm_updated_at' => date('Y-m-d H:i:s'),
                'm_updated_by' => $this->session->userdata['u_name']
            ];

            $this->m_majors->edit($data, $m_id);
            $this->session->set_flashdata('alert', success('Data Program Studi berhasil diperbarui.'));
            $data['title'] = "Data ".self::$title;
            $data['content'] = "dashboard/prodi";
            redirect('prodi');

        } else {
            $where = "m_id = '$m_id'";

            $data['prodi'] = $this->m_majors->get_majors($where);
            $data['title'] = "Edit ".self::$title;
            $data['form_title'] = "Edit Program Studi";
            $data['action'] = site_url(uri_string());
            $data['content'] = 'dashboard/prodi-form';
            if (!$m_id) {
                redirect('prodi');
            } else {
                $this->load->view('dashboard/index', $data);
            }
        }
    }

    public function delete($m_id)
    {
        $this->load->helper('notification');

        $data = [
            'm_deleted_at' => date('Y-m-d H:i:s'),
            'm_deleted_by' => $this->session->userdata['u_name'],
            'm_is_deleted' => TRUE
        ];

        $this->m_majors->delete($data, $m_id);
        $this->session->set_flashdata('alert', success('Data Program Studi berhasil dihapus.'));
        $data['title'] = "Data ".self::$title;
        $data['content'] = "dashboard/prodi";
        $this->load->view('dashboard/index', $data);
        redirect('prodi');
    }

    public function deleted()
    {
        $data['title'] = "Data ".self::$title;
        $data['content'] = "dashboard/prodi-deleted";
        $this->load->view('dashboard/index', $data);
    }

    public function get_deleted()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            $this->load->library('datatables_ssp');
            $columns = array(
                array('db' => 'm_id', 'dt' => 'm_id'),
                array('db' => 'prodi', 'dt' => 'prodi'),
                array('db' => 'm_created_at', 'dt' => 'm_created_at'),
                array('db' => 'm_deleted_at', 'dt' => 'm_deleted_at'),
                array(
                    'db' => 'm_id',
                    'dt' => 'tindakan',
                    'formatter' => function($m_id) {
                        return '<a class="btn btn-success btn-sm" onclick="return confirmDialog();" href="'.site_url('prodi/restore/'.$m_id).'"><span class="glyphicon glyphicon-repeat" aria-hidden="true"></span> Restore</a>
                            <a class="btn btn-danger btn-sm" onclick="return confirmDialogDelete();" href="'.site_url('prodi/deletex/'.$m_id).'"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Hapus</a>';
                    }
                ),
            );

            $sql_details = [
                'user' => $this->db->username,
                'pass' => $this->db->password,
                'db' => $this->db->database,
                'host' => $this->db->hostname
            ];

            echo json_encode(
                Datatables_ssp::complex($_GET, $sql_details, self::$table, self::$primaryKey, $columns, NULL, "m_is_deleted = 'TRUE'")
            );
        }
    }

    public function restore()
    {
        $this->load->helper('notification');
        $m_id = $this->uri->segment(3);

        $data = [
			'm_restored_at' => date('Y-m-d H:i:s'),
			'm_restored_by' => $this->session->userdata['u_name'],
			'm_is_deleted' => 'FALSE'
		];

        $this->m_majors->restore($data, $m_id);
        $this->session->set_flashdata('alert', success('Data Program Studi berhasil direstore.'));
        $data['title'] = "Data ".self::$title;
        $data['content'] = "dashboard/prodi-deleted";
        redirect('prodi/deleted');
    }

    private function validation()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('prodi', 'Nama Program Studi', 'trim|required|min_length[5]|max_length[50]|xss_clean');
        $this->form_validation->set_rules('m_id', 'Kode Program Studi', 'trim|required|alpha|max_length[5]|xss_clean');
        return $this->form_validation->run();
    }

    public function deletex($m_id)
    {
        $sql = $this->m_majors->deletex($m_id);
        if($sql == "success"){
            redirect(site_url('prodi/deleted'));
        }else{
            redirect(site_url('prodi/deleted'));
        }
    }

}
