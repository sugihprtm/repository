<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Lihat_skripsi extends CI_Controller {

 	public function __construct()
    {
        parent::__construct(); 
        $this->load->model('m_homes');
        $this->auth->auth();
    }  
 
	public function index()
	{
        $this->load->helper('form');
        $data['title'] = "Data Skripsi Mahasiswa &minus; SISTEM INFORMASI TUGAS AKHIR - FASILKOM UNSIKA";
        $data['content'] = 'dashboard/lihat_skripsi';
		$this->load->view('dashboard/index', $data);
	}

    public function result() 
    {
        $keyword = $this->input->get('keyword', TRUE);

        if (is_null($keyword)) {
            $this->load->helper('form');
        } else {
            $this->load->model('m_homes');
            $this->load->helper('form');
            $data['result'] = $this->m_homes->get_result($keyword);
            $data['keyword'] = $keyword;
            $this->load->view('dashboard/result_skripsi', $data);
        }
    }

    public function view($npm)
    {
        $this->load->helper('form');
        $npm = $this->uri->segment(3);
        $data['student'] = $this->m_homes->get_data($npm);
        $data['title'] = $data['student']['nama']." &minus; SISTEM INFORMASI TUGAS AKHIR - FASILKOM UNSIKA";
        $data['attachment'] = 'Lampiran';
        $data['content'] = 'result-view';

        if (!$npm) {
            redirect(site_url('index', $data));
        } else { 
            $this->load->view('index', $data);
        }
    }

    public function print_data()
    {
        $s_id = $this->uri->segment(3);
        $data['student'] = $this->m_homes->get_data($s_id);
        $data['title'] = $data['student']['s_name']." &minus; SISTEM INFORMASI TUGAS AKHIR - FASILKOM UNSIKA";
        $data['attachment'] = 'Lampiran';
        if (!$s_id) {
            redirect(site_url('home'));
        } else {
            $this->load->view('result-print', $data);
        }
    }
}
