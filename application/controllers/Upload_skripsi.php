<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Upload_skripsi extends CI_Controller {

 	public function __construct()
    {
        parent::__construct(); 
        $this->load->model('m_students');
        $this->auth->restrict();
    }

    private static $title = "Skripsi Mahasiswa &minus; REPOSITORY TUGAS AKHIR - FASILKOM UNSIKA";
    private static $table = 'students';
    private static $primaryKey = 'npm';

    public function index()
	{   
        
        $npm = $this->session->userdata['u_name'];
        $where = "npm = '$npm'";
        $data['student'] = $this->m_students->get_students($where);
        $data['attachment'] = 'Lampiran';

        $data['title'] = "Data ".self::$title;
        $data['content'] = "dashboard/upload_skripsi";
        $this->load->view('dashboard/index', $data);

    }

    private function validation()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('judul', 'Judul', 'trim|required|xss_clean');
        $this->form_validation->set_rules('abstrak_judul', 'Abstrak', 'trim|required|xss_clean');
        $this->form_validation->set_rules('pembimbing1', 'Pembimbing1', 'trim|required|max_length[50]|xss_clean');
        $this->form_validation->set_rules('pembimbing2', 'Pembimbing2', 'trim|required|max_length[50]|xss_clean');
        return $this->form_validation->run();
    }

    public function edit()
    {
        $this->load->helper(['form', 'notification']);
        //$this->session->userdata['u_name'],
        $npm = $this->session->userdata['u_name'];
        $where = "npm = '$npm'";
        $data['student'] = $this->m_students->get_students($where);


        if ($this->validation()) {

            $daftarisi = $_FILES['daftarisi']['name'];
            $abstrak = $_FILES['abstrak']['name'];
            $bab1 = $_FILES['bab1']['name'];
            $bab2 = $_FILES['bab2']['name'];
            $bab3 = $_FILES['bab3']['name'];
            $bab4 = $_FILES['bab4']['name'];
            $bab5 = $_FILES['bab5']['name'];
            $daftarpustaka = $_FILES['daftarpustaka']['name'];
            $lampiran = $_FILES['lampiran']['name'];
            $fullskripsi = $_FILES['fullskripsi']['name'];
            $sourcecode = $_FILES['sourcecode']['name'];
            $program = $_FILES['program']['name'];

            $this->load->library('upload');

            if (!empty($daftarisi)) {
                $config['upload_path'] = './uploads/daftarisi/';
                $config['allowed_types'] = 'pdf';
                $config['file_ext_tolower'] = TRUE;
                $config['max_size'] = '2048';
                $config['overwrite'] = TRUE;
                $x = explode(".", $daftarisi);
                $ext = strtolower(end($x));
                $config['file_name'] = $npm."-daftarisi.".$ext;
                $daftarisi = $config['file_name'];
                $this->upload->initialize($config);
                $this->upload->do_upload('daftarisi');
            }

            if (!empty($abstrak)) {
                $config['upload_path'] = './uploads/abstrak/';
                $config['allowed_types'] = 'pdf';
                $config['file_ext_tolower'] = TRUE;
                $config['max_size'] = '2048';
                $config['overwrite'] = TRUE;
                $x = explode(".", $abstrak);
                $ext = strtolower(end($x));
                $config['file_name'] = $npm."-abstrak.".$ext;
                $abstrak = $config['file_name'];
                $this->upload->initialize($config);
                $this->upload->do_upload('abstrak');
            }

            if (!empty($bab1)) {
                $config['upload_path'] = './uploads/bab1/';
                $config['allowed_types'] = 'pdf';
                $config['file_ext_tolower'] = TRUE;
                $config['max_size'] = '2048';
                $config['overwrite'] = TRUE;
                $x = explode(".", $bab1);
                $ext = strtolower(end($x));
                $config['file_name'] = $npm."-bab1.".$ext;
                $bab1 = $config['file_name'];
                $this->upload->initialize($config);
                $this->upload->do_upload('bab1');
            }

            if (!empty($bab2)) {
                $config['upload_path'] = './uploads/bab2/';
                $config['allowed_types'] = 'pdf';
                $config['file_ext_tolower'] = TRUE;
                $config['max_size'] = '2048';
                $config['overwrite'] = TRUE;
                $x = explode(".", $bab2);
                $ext = strtolower(end($x));
                $config['file_name'] = $npm."-bab2.".$ext;
                $bab2 = $config['file_name'];
                $this->upload->initialize($config);
                $this->upload->do_upload('bab2');
            }

            if (!empty($bab3)) {
                $config['upload_path'] = './uploads/bab3/';
                $config['allowed_types'] = 'pdf';
                $config['file_ext_tolower'] = TRUE;
                $config['max_size'] = '2048';
                $config['overwrite'] = TRUE;
                $x = explode(".", $bab3);
                $ext = strtolower(end($x));
                $config['file_name'] = $npm."-bab3.".$ext;
                $bab3 = $config['file_name'];
                $this->upload->initialize($config);
                $this->upload->do_upload('bab3');
            }

            if (!empty($bab4)) {
                $config['upload_path'] = './uploads/bab4/';
                $config['allowed_types'] = 'pdf';
                $config['file_ext_tolower'] = TRUE;
                $config['max_size'] = '2048';
                $config['overwrite'] = TRUE;
                $x = explode(".", $bab4);
                $ext = strtolower(end($x));
                $config['file_name'] = $npm."-bab4.".$ext;
                $bab4 = $config['file_name'];
                $this->upload->initialize($config);
                $this->upload->do_upload('bab4');
            }

            if (!empty($bab5)) {
                $config['upload_path'] = './uploads/bab5/';
                $config['allowed_types'] = 'pdf';
                $config['file_ext_tolower'] = TRUE;
                $config['max_size'] = '2048';
                $config['overwrite'] = TRUE;
                $x = explode(".", $bab5);
                $ext = strtolower(end($x));
                $config['file_name'] = $npm."-bab5.".$ext;
                $bab5 = $config['file_name'];
                $this->upload->initialize($config);
                $this->upload->do_upload('bab5');
            }

            if (!empty($daftarpustaka)) {
                $config['upload_path'] = './uploads/daftarpustaka/';
                $config['allowed_types'] = 'pdf';
                $config['file_ext_tolower'] = TRUE;
                $config['max_size'] = '2048';
                $config['overwrite'] = TRUE;
                $x = explode(".", $daftarpustaka);
                $ext = strtolower(end($x));
                $config['file_name'] = $npm."-daftarpustaka.".$ext;
                $daftarpustaka = $config['file_name'];
                $this->upload->initialize($config);
                $this->upload->do_upload('daftarpustaka');
            }

            if (!empty($lampiran)) {
                $config['upload_path'] = './uploads/lampiran/';
                $config['allowed_types'] = 'pdf';
                $config['file_ext_tolower'] = TRUE;
                $config['max_size'] = '2048';
                $config['overwrite'] = TRUE;
                $x = explode(".", $lampiran);
                $ext = strtolower(end($x));
                $config['file_name'] = $npm."-lampiran.".$ext;
                $lampiran = $config['file_name'];
                $this->upload->initialize($config);
                $this->upload->do_upload('lampiran');
            }

            if (!empty($fullskripsi)) {
                $config['upload_path'] = './uploads/fullskripsi/';
                $config['allowed_types'] = 'pdf';
                $config['file_ext_tolower'] = TRUE;
                $config['max_size'] = '2048';
                $config['overwrite'] = TRUE;
                $x = explode(".", $fullskripsi);
                $ext = strtolower(end($x));
                $config['file_name'] = $npm."-fullskripsi.".$ext;
                $fullskripsi = $config['file_name'];
                $this->upload->initialize($config);
                $this->upload->do_upload('fullskripsi');
            }

            if (!empty($sourcecode)) {
                $config['upload_path'] = './uploads/sourcecode/';
                $config['allowed_types'] = 'rar|zip|7z';
                $config['file_ext_tolower'] = TRUE;
                $config['max_size'] = '10000';
                $config['overwrite'] = TRUE;
                $x = explode(".", $sourcecode);
                $ext = strtolower(end($x));
                $config['file_name'] = $npm."-sourcecode.".$ext;
                $sourcecode = $config['file_name'];
                $this->upload->initialize($config);
                $this->upload->do_upload('sourcecode');
            }

            if (!empty($program)) {
                $config['upload_path'] = './uploads/program/';
                $config['allowed_types'] = 'rar|zip|7z';
                $config['file_ext_tolower'] = TRUE;
                $config['max_size'] = '10000';
                $config['overwrite'] = TRUE;
                $x = explode(".", $program);
                $ext = strtolower(end($x));
                $config['file_name'] = $npm."-program.".$ext;
                $program = $config['file_name'];
                $this->upload->initialize($config);
                $this->upload->do_upload('program');
            }


            if (!empty($daftarisi) && !$this->upload->do_upload('daftarisi')) {
                $data['err_daftarisi'] = $this->upload->display_errors();
                $where = "m_is_deleted = 'False'";
                $data['majors'] = $this->m_students->get_majors($where);
                $data['title'] = "Tambah ".self::$title;
                $data['form_title'] = "Tambah Skripsi Mahasiswa";
                $data['action'] = site_url(uri_string());
                $data['content'] = 'dashboard/upload_skripsi_edit';
                $this->load->view('dashboard/index', $data);

            } else if (!empty($abstrak) && !$this->upload->do_upload('abstrak')) {
                $data['err_abstrak'] = $this->upload->display_errors();
                $where = "m_is_deleted = 'False'";
                $data['majors'] = $this->m_students->get_majors($where);
                $data['title'] = "Tambah ".self::$title;
                $data['form_title'] = "Tambah Skripsi Mahasiswa";
                $data['action'] = site_url(uri_string());
                $data['content'] = 'dashboard/upload_skripsi_edit';
                $this->load->view('dashboard/index', $data);

            } else if (!empty($bab1) && !$this->upload->do_upload('bab1')) {
                $data['err_bab1'] = $this->upload->display_errors();
                $where = "m_is_deleted = 'False'";
                $data['majors'] = $this->m_students->get_majors($where);
                $data['title'] = "Tambah ".self::$title;
                $data['form_title'] = "Tambah Skripsi Mahasiswa";
                $data['action'] = site_url(uri_string());
                $data['content'] = 'dashboard/upload_skripsi_edit';
                $this->load->view('dashboard/index', $data);

            } else if (!empty($bab2) && !$this->upload->do_upload('bab2')) {
                $data['err_bab2'] = $this->upload->display_errors();
                $where = "m_is_deleted = 'False'";
                $data['majors'] = $this->m_students->get_majors($where);
                $data['title'] = "Tambah ".self::$title;
                $data['form_title'] = "Tambah Skripsi Mahasiswa";
                $data['action'] = site_url(uri_string());
                $data['content'] = 'dashboard/upload_skripsi_edit';
                $this->load->view('dashboard/index', $data);

            } else if (!empty($bab3) && !$this->upload->do_upload('bab3')) {
                $data['err_bab3'] = $this->upload->display_errors();
                $where = "m_is_deleted = 'False'";
                $data['majors'] = $this->m_students->get_majors($where);
                $data['title'] = "Tambah ".self::$title;
                $data['form_title'] = "Tambah Skripsi Mahasiswa";
                $data['action'] = site_url(uri_string());
                $data['content'] = 'dashboard/upload_skripsi_edit';
                $this->load->view('dashboard/index', $data);

            } else if (!empty($bab4) && !$this->upload->do_upload('bab4')) {
                $data['err_bab4'] = $this->upload->display_errors();
                $where = "m_is_deleted = 'False'";
                $data['majors'] = $this->m_students->get_majors($where);
                $data['title'] = "Tambah ".self::$title;
                $data['form_title'] = "Tambah Skripsi Mahasiswa";
                $data['action'] = site_url(uri_string());
                $data['content'] = 'dashboard/upload_skripsi_edit';
                $this->load->view('dashboard/index', $data);

            } else if (!empty($bab5) && !$this->upload->do_upload('bab5')) {
                $data['err_bab5'] = $this->upload->display_errors();
                $where = "m_is_deleted = 'False'";
                $data['majors'] = $this->m_students->get_majors($where);
                $data['title'] = "Tambah ".self::$title;
                $data['form_title'] = "Tambah Skripsi Mahasiswa";
                $data['action'] = site_url(uri_string());
                $data['content'] = 'dashboard/upload_skripsi_edit';
                $this->load->view('dashboard/index', $data);

            } else if (!empty($daftarpustaka) && !$this->upload->do_upload('daftarpustaka')) {
                $data['err_daftarpustaka'] = $this->upload->display_errors();
                $where = "m_is_deleted = 'False'";
                $data['majors'] = $this->m_students->get_majors($where);
                $data['title'] = "Tambah ".self::$title;
                $data['form_title'] = "Tambah Skripsi Mahasiswa";
                $data['action'] = site_url(uri_string());
                $data['content'] = 'dashboard/upload_skripsi_edit';
                $this->load->view('dashboard/index', $data);

            } else if (!empty($lampiran) && !$this->upload->do_upload('lampiran')) {
                $data['err_lampiran'] = $this->upload->display_errors();
                $where = "m_is_deleted = 'False'";
                $data['majors'] = $this->m_students->get_majors($where);
                $data['title'] = "Tambah ".self::$title;
                $data['form_title'] = "Tambah Skripsi Mahasiswa";
                $data['action'] = site_url(uri_string());
                $data['content'] = 'dashboard/upload_skripsi_edit';
                $this->load->view('dashboard/index', $data);

            } else if (!empty($fullskripsi) && !$this->upload->do_upload('fullskripsi')) {
                $data['err_fullskripsi'] = $this->upload->display_errors();
                $where = "m_is_deleted = 'False'";
                $data['majors'] = $this->m_students->get_majors($where);
                $data['title'] = "Tambah ".self::$title;
                $data['form_title'] = "Tambah Skripsi Mahasiswa";
                $data['action'] = site_url(uri_string());
                $data['content'] = 'dashboard/upload_skripsi_edit';
                $this->load->view('dashboard/index', $data);

            } else if (!empty($sourcecode) && !$this->upload->do_upload('sourcecode')) {
                $data['err_sourcecode'] = $this->upload->display_errors();
                $where = "m_is_deleted = 'False'";
                $data['majors'] = $this->m_students->get_majors($where);
                $data['title'] = "Tambah ".self::$title;
                $data['form_title'] = "Tambah Skripsi Mahasiswa";
                $data['action'] = site_url(uri_string());
                $data['content'] = 'dashboard/upload_skripsi_edit';
                $this->load->view('dashboard/index', $data);

            } else if (!empty($program) && !$this->upload->do_upload('program')) {
                $data['err_program'] = $this->upload->display_errors();
                $where = "m_is_deleted = 'False'";
                $data['majors'] = $this->m_students->get_majors($where);
                $data['title'] = "Tambah ".self::$title;
                $data['form_title'] = "Tambah Skripsi Mahasiswa";
                $data['action'] = site_url(uri_string());
                $data['content'] = 'dashboard/upload_skripsi_edit';
                $this->load->view('dashboard/index', $data);

            }

            else {

                $data = [
                    
                    'judul' => $this->input->post('judul', TRUE),
                    'abstrak_judul' => $this->input->post('abstrak_judul', TRUE),
                    'pembimbing1' => $this->input->post('pembimbing1', TRUE),
                    'pembimbing2' => $this->input->post('pembimbing2', TRUE),
                    'daftarisi' => (!empty($daftarisi)) ? $daftarisi : $data['student']['daftarisi'],
                    'abstrak' => (!empty($abstrak)) ? $abstrak : $data['student']['abstrak'],
                    'bab1' => (!empty($bab1)) ? $bab1 : $data['student']['bab1'],
                    'bab2' => (!empty($bab2)) ? $bab2 : $data['student']['bab2'],
                    'bab3' => (!empty($bab3)) ? $bab3 : $data['student']['bab3'],
                    'bab4' => (!empty($bab4)) ? $bab4 : $data['student']['bab4'],
                    'bab5' => (!empty($bab5)) ? $bab5 : $data['student']['bab5'],
                    'daftarpustaka' => (!empty($daftarpustaka)) ? $daftarpustaka : $data['student']['daftarpustaka'],
                    'lampiran' => (!empty($lampiran)) ? $lampiran : $data['student']['lampiran'],
                    'fullskripsi' => (!empty($fullskripsi)) ? $fullskripsi : $data['student']['fullskripsi'],
                    'sourcecode' => (!empty($sourcecode)) ? $sourcecode : $data['student']['sourcecode'],
                    'program' => (!empty($program)) ? $program : $data['student']['program'],
                    's_updated_at' => date('Y-m-d H:i:s'),
                    's_updated_by' => $this->session->userdata['u_name']
                ];

                $this->m_students->edit($data, $npm);
                $this->session->set_flashdata('alert', success('Data berhasil disimpan.'));
                $data['title'] = "Data ".self::$title;
                $data['content'] = "dashboard/upload_skripsi";
                redirect(site_url('upload_skripsi'));
            }
        } 
        else {
            $data['dosen'] = $this->m_students->get_dosen();
            $data['title'] = "Tambah ".self::$title;
            $data['form_title'] = "Tambah Data ".$data['student']['nama'] ;
            $data['action'] = site_url(uri_string());
            $data['content'] = 'dashboard/upload_skripsi_edit';
            if (!$npm) {
                redirect(site_url('upload_skripsi'));
            } else {
                $this->load->view('dashboard/index', $data);
            }
        }
    }

    public function print_data()
    {
        $npm = $this->session->userdata['u_name'];

        $where = "npm = '$npm'";

        $data['student'] = $this->m_students->get_students($where);
        $data['title'] = $data['student']['s_nama']." &minus; REPOSITORY TUGAS AKHIR - FASILKOM UNSIKA";
        $data['attachment'] = 'Lampiran';
        if (!$npm) {
            redirect(site_url('upload_skripsi'));
        } else {
            $this->load->view('dashboard/upload_skripsi', $data);
        }
    }
    
}
