<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen extends CI_Controller {

 	public function __construct()
    {
        parent::__construct(); 
        $this->load->model('m_dosen');
        $this->auth->restrict();
        $this->auth->admin();
    }

    private static $title = "Dosen &minus; SISTEM INFORMASI TUGAS AKHIR - FASILKOM UNSIKA";
    private static $table = 'dosen';
	private static $primaryKey = 'nidn';

    public function index()
	{
        $data['title'] = "Data ".self::$title;
        $data['content'] = "dashboard/dosen";
		$this->load->view('dashboard/index', $data);
	}

    public function get_data()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            $this->load->library('datatables_ssp');
            $columns = array(
                array('db' => 'nidn', 'dt' => 'nidn'),
                array('db' => 'nama', 'dt' => 'nama'),
                array('db' => 'd_created_at', 'dt' => 'd_created_at'),
                array('db' => 'd_updated_at', 'dt' => 'd_updated_at'),
                array(
                    'db' => 'nidn',
                    'dt' => 'tindakan',
                    'formatter' => function($nidn) {
                        return '<a class="btn btn-info btn-sm mb" href="'.site_url('dosen/edit/'.$nidn).'">Ubah</a>
                        <a class="btn btn-danger btn-sm mb" onclick="return confirmDialog();" href="'.site_url('dosen/delete/'.$nidn).'">Hapus</a>';
                    }
                ),
            );

            $sql_details = [
                'user' => $this->db->username,
                'pass' => $this->db->password,
                'db' => $this->db->database,
                'host' => $this->db->hostname
            ];

            echo json_encode(
                Datatables_ssp::complex($_GET, $sql_details, self::$table, self::$primaryKey, $columns, NULL, "d_is_deleted = 'FALSE'")
            );
        }
    }

    public function add()
    {
        $this->load->helper(['form', 'notification']);

        if ($this->validation()) {

            $nidn = $this->input->post('nidn', TRUE);
            $nama = $this->input->post('nama', TRUE);
            $where = "nidn = '$nidn' OR nama = '$nama'";

            $data = $this->m_dosen->is_exist($where);
            if (strtolower($data['nama']) === strtolower($this->input->post('nama', TRUE))) {
                $this->session->set_flashdata('alert', error('Nama Dosen sudah ada!'));
                $data['title'] = "Tambah ".self::$title;
                $data['form_title'] = "Tambah Dosen";
                $data['action'] = site_url(uri_string());
                $data['content'] = 'dashboard/dosen-form';
                $this->load->view('dashboard/index', $data);

            } else if (strtolower($data['nidn']) === strtolower($this->input->post('nidn', TRUE))) {
                $this->session->set_flashdata('alert', error('NIDN Dosen sudah ada!'));
                $data['title'] = "Tambah ".self::$title;
                $data['form_title'] = "Tambah Dosen";
                $data['action'] = site_url(uri_string());
                $data['content'] = 'dashboard/dosen-form';
                $this->load->view('dashboard/index', $data);

            } else {
                $data = [
                    'nidn' => strtoupper($this->input->post('nidn', TRUE)),
                    'nama' => ucfirst($this->input->post('nama', TRUE)),
                    'd_created_by' => $this->session->userdata['u_name'],
                    'd_is_deleted' => 'FALSE'
                ];

                $this->m_dosen->add($data);
                $this->session->set_flashdata('alert', success('Data Dosen berhasil ditambahkan.'));
                $data['title'] = "Data ".self::$title;
                $data['content'] = "dashboard/dosen";
                redirect('dosen');
            }

        } else {
            $data['dosen'] = FALSE;
            $data['title'] = "Tambah ".self::$title;
            $data['form_title'] = "Tambah Dosen";
            $data['action'] = site_url(uri_string());
            $data['content'] = 'dashboard/dosen-form';
            $this->load->view('dashboard/index', $data);
        }
    }

    public function edit()
    {
        $this->load->helper(['form', 'notification']);
        $nidn = $this->uri->segment(3);

        if ($this->validation()) {

            $data = [
                'nidn' => strtoupper($this->input->post('nidn', TRUE)),
                'nama' => ucfirst($this->input->post('nama', TRUE)),
                'd_updated_at' => date('Y-m-d H:i:s'),
                'd_updated_by' => $this->session->userdata['u_name']
            ];

            $this->m_dosen->edit($data, $nidn);
            $this->session->set_flashdata('alert', success('Data Dosen berhasil diperbarui.'));
            $data['title'] = "Data ".self::$title;
            $data['content'] = "dashboard/dosen";
            redirect('dosen');

        } else {
            $where = "nidn = '$nidn'";

            $data['dosen'] = $this->m_dosen->get_dosen($where);
            $data['title'] = "Edit ".self::$title;
            $data['form_title'] = "Edit Dosen";
            $data['action'] = site_url(uri_string());
            $data['content'] = 'dashboard/dosen-form';
            if (!$nidn) {
                redirect('dosen');
            } else {
                $this->load->view('dashboard/index', $data);
            }
        }
    }

    public function delete($nidn)
    {
        $this->load->helper('notification');

        $data = [
            'd_deleted_at' => date('Y-m-d H:i:s'),
            'd_deleted_by' => $this->session->userdata['u_name'],
            'd_is_deleted' => TRUE
        ];

        $this->m_dosen->delete($data, $nidn);
        $this->session->set_flashdata('alert', success('Data Dosen berhasil dihapus.'));
        $data['title'] = "Data ".self::$title;
        $data['content'] = "dashboard/dosen";
        $this->load->view('dashboard/index', $data);
        redirect('dosen');
    }

    public function deleted()
    {
        $data['title'] = "Data ".self::$title;
        $data['content'] = "dashboard/dosen-deleted";
        $this->load->view('dashboard/index', $data);
    }

    public function get_deleted()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            $this->load->library('datatables_ssp');
            $columns = array(
                array('db' => 'nidn', 'dt' => 'nidn'),
                array('db' => 'nama', 'dt' => 'nama'),
                array('db' => 'd_created_at', 'dt' => 'd_created_at'),
                array('db' => 'd_deleted_at', 'dt' => 'd_deleted_at'),
                array(
                    'db' => 'nidn',
                    'dt' => 'tindakan',
                    'formatter' => function($nidn) {
                        return '<a class="btn btn-success btn-sm" onclick="return confirmDialog();" href="'.site_url('dosen/restore/'.$nidn).'"><span class="glyphicon glyphicon-repeat" aria-hidden="true"></span> Restore</a>
                            <a class="btn btn-danger btn-sm" onclick="return confirmDialogDelete();" href="'.site_url('dosen/deletex/'.$nidn).'"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Hapus</a>';
                    }
                ),
            );

            $sql_details = [
                'user' => $this->db->username,
                'pass' => $this->db->password,
                'db' => $this->db->database,
                'host' => $this->db->hostname
            ];

            echo json_encode(
                Datatables_ssp::complex($_GET, $sql_details, self::$table, self::$primaryKey, $columns, NULL, "d_is_deleted = 'TRUE'")
            );
        }
    }

    public function restore()
    {
        $this->load->helper('notification');
        $nidn = $this->uri->segment(3);

        $data = [
			'd_restored_at' => date('Y-m-d H:i:s'),
			'd_restored_by' => $this->session->userdata['u_name'],
			'd_is_deleted' => 'FALSE'
		];

        $this->m_dosen->restore($data, $nidn);
        $this->session->set_flashdata('alert', success('Data Dosen berhasil direstore.'));
        $data['title'] = "Data ".self::$title;
        $data['content'] = "dashboard/dosen-deleted";
        redirect('dosen/deleted');
    }

    private function validation()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nidn', 'NIDN Dosen', 'trim|required|min_length[10]|max_length[14]|xss_clean');
        $this->form_validation->set_rules('nama', 'Nama Dosen', 'trim|required|max_length[50]|xss_clean');
        return $this->form_validation->run();
    }

    public function deletex($nidn)
    {
        $sql = $this->m_dosen->deletex($nidn);
        if($sql == "success"){
            redirect(site_url('dosen/deleted'));
        }else{
            redirect(site_url('dosen/deleted'));
        }
    }

}
