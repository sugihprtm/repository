<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {

 	public function __construct() 
    {
        parent::__construct();
        $this->load->model('m_students');
        $this->auth->restrict();
    }

    private static $title = "Profil Mahasiswa &minus; SISTEM INFORMASI TUGAS AKHIR - FASILKOM UNSIKA";
    private static $table = 'students';
    private static $primaryKey = 'npm';

    public function index()
	{
        
        $npm = $this->session->userdata['u_name'];
        $where = "npm = '$npm'";
        $data['student'] = $this->m_students->get_students($where);
        $data['attachment'] = 'Lampiran';

        $data['title'] = "Data ".self::$title;
        $data['content'] = "dashboard/profil";
        $this->load->view('dashboard/index', $data);

    }

    private function validation()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('npm', 'NPM', 'trim|required|min_length[10]|max_length[14]|xss_clean');
        $this->form_validation->set_rules('nama', 'Nama Mahasiswa', 'trim|required|min_length[3]|max_length[50]|xss_clean');
        $this->form_validation->set_rules('pob', 'Tempat Lahir', 'trim|required|xss_clean');
        $this->form_validation->set_rules('dob', 'Tanggal Lahir', 'trim|required|xss_clean');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'trim|required|xss_clean');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required|max_length[100]|xss_clean');
        $this->form_validation->set_rules('prodi', 'Program Keahlian', 'trim|required|max_length[20]|xss_clean');
        $this->form_validation->set_rules('no_hp', 'Nomor Handphone', 'trim|required|max_length[13]|xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[50]|xss_clean');
        return $this->form_validation->run();
    }

    public function edit()
    {
        $this->load->helper(['form', 'notification']);
        //$this->session->userdata['u_name'],
        $npm = $this->session->userdata['u_name'];
        $where = "npm = '$npm'";
        $data['student'] = $this->m_students->get_students($where);

        if ($this->validation()) {

            $foto = $_FILES['foto']['name'];

            $this->load->library('upload');

            if (!empty($foto)) {
                $config['upload_path'] = './uploads/foto/';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_ext_tolower'] = TRUE;
                $config['max_size'] = '1024';
                $config['overwrite'] = TRUE;
                $x = explode(".", $foto);
                $ext = strtolower(end($x));
                $config['file_name'] = $npm."-foto.".$ext;
                $foto = $config['file_name'];
                $this->upload->initialize($config);
                $this->upload->do_upload('foto');
            }


            if (!empty($foto) && !$this->upload->do_upload('foto')) {
                $data['err_foto'] = $this->upload->display_errors();
                $where = "m_is_deleted = 'False'";
                $data['majors'] = $this->m_students->get_majors($where);
                $data['title'] = "Tambah ".self::$title;
                $data['form_title'] = "Tambah Mahasiswa";
                $data['action'] = site_url(uri_string());
                $data['content'] = 'dashboard/profil_edit';
                $this->load->view('dashboard/index', $data);

            } 

            else {

                $data = [
                    'npm' => $npm,
                    'npm' => $this->input->post('npm', TRUE),
                    'nama' => $this->input->post('nama', TRUE),
                    'pob' => $this->input->post('pob', TRUE),
                    'dob' => $this->input->post('dob', TRUE),
                    'jenis_kelamin' => $this->input->post('jenis_kelamin', TRUE),
                    'alamat' => $this->input->post('alamat', TRUE),
                    'prodi' => $this->input->post('prodi', TRUE),
                    'no_hp' => $this->input->post('no_hp', TRUE),
                    'email' => $this->input->post('email', TRUE),
                    'foto' => (!empty($foto)) ? $foto : $data['student']['foto'],
                    's_status' => $this->input->post('s_status', TRUE),
                    's_updated_at' => date('Y-m-d H:i:s'),
                    's_updated_by' => $this->session->userdata['u_name'],
                ];

                $this->m_students->edit($data, $npm);
                $this->session->set_flashdata('alert', success('Data berhasil disimpan.'));
                $data['title'] = "Data ".self::$title;
                $data['content'] = "dashboard/profil";
                redirect(site_url('profil'));
            }
        } 
        else {
            $data['majors'] = $this->m_students->get_majors();
            $data['title'] = "Edit ".self::$title;
            $data['form_title'] = "Edit Data ".$data['student']['nama'] ;
            $data['action'] = site_url(uri_string());
            $data['content'] = 'dashboard/profil_edit';
            if (!$npm) {
                redirect(site_url('profil'));
            } else {
                $this->load->view('dashboard/index', $data);
            }
        }
    }

    public function print_data()
    {
        $npm = $this->session->userdata['u_name'];

        $where = "npm = '$npm'";

        $mhs['student'] = $this->m_students->get_students($where);
        $mhs['title'] = $mhs['student']['nama']." &minus; SISTEM INFORMASI TUGAS AKHIR - FASILKOM UNSIKA";
        $mhs['attachment'] = 'Lampiran';
        if (!$npm) {
            redirect(site_url('profil'));
        } else {
            $this->load->view('dashboard/student-print', $mhs);
        }
    }

    public function get_data()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            $this->load->library('datatables_ssp');
            $columns = array(
                array('db' => 'npm', 'dt' => 'npm'),
                array('db' => 'nama', 'dt' => 'nama'),
                array('db' => 'pob', 'dt' => 'pob'),
                array('db' => 'dob', 'dt' => 'dob'),
                array('db' => 'jenis_kelamin', 'dt' => 'jenis_kelamin'),
                array('db' => 'alamat', 'dt' => 'alamat'),
                array('db' => 'prodi', 'dt' => 'prodi'),
                array('db' => 'no_hp', 'dt' => 'no_hp'),
                array('db' => 'email', 'dt' => 'email'),
                array(
                    'db' => 'npm',
                    'dt' => 'tindakan',
                    'formatter' => function($npm) {
                        return '<a class="btn btn-warning btn-sm mb" href="'.site_url('profil/print_data/'.$npm).'" target="_blank" title="Cetak data"><span class="glyphicon glyphicon-print" aria-hidden="true"></span></a>
                        <a class="btn btn-success btn-sm mb" title="Lihat Data" href="'.site_url('profil/view/'.$npm).'">Lihat</a>
                        <a class="btn btn-info btn-sm mb" title="Edit Data" href="'.site_url('profilt/edit/'.$npm).'">Edit</a>
                        <a class="btn btn-danger btn-sm mb" onclick="return confirmDialog();" href="'.site_url('profil/delete/'.$npm).'" title="Hapus Data"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                        <a class="btn btn-default btn-sm mb" title="Ubah Status Mahasiswa" href="'.site_url('profil/status/'.$npm).'" onclick="return confirmDialogStatus();">Ubah Status</a>';
                    }
                ),
            );

            $sql_details = [
                'user' => $this->db->username,
                'pass' => $this->db->password,
                'db' => $this->db->database,
                'host' => $this->db->hostname
            ];


            echo json_encode(
                Datatables_ssp::complex($_GET, $sql_details, self::$table, self::$primaryKey, $columns, NULL, "s_is_active = 'Aktif' AND s_is_deleted = 'FALSE'")
            );
        }
    }
    
}
