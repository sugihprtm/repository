<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cari_skripsi extends CI_Controller {

 	public function __construct() 
    {
        parent::__construct();
        $this->load->model('m_students');
        $this->auth->restrict();
    }

    private static $title = "Pencarian Skripsi Mahasiswa &minus; SISTEM INFORMASI TUGAS AKHIR - FASILKOM UNSIKA";
    private static $table = 'students';
    private static $primaryKey = 'npm';

    public function index()
	{
        $where = "s_status = 'Selesai'";
        $data['student'] = $this->m_students->get_students($where);
        $data['attachment'] = 'Lampiran';

        $data['title'] = "Data ".self::$title;
        $data['content'] = "dashboard/cari_skripsi";
        $this->load->view('dashboard/index', $data);

    }

    public function print_data()
    {
        $npm = $this->uri->segment(3);

        $where = "npm = '$npm'";

        $mhs['student'] = $this->m_students->get_students($where);
        $mhs['title'] = $mhs['student']['nama']." &minus; SISTEM INFORMASI TUGAS AKHIR - FASILKOM UNSIKA";
        $mhs['attachment'] = 'Lampiran';
        if (!$npm) {
            redirect(site_url('cari_skripsi'));
        } else {
            $this->load->view('dashboard/cari_skripsi-print', $mhs);
        }
    }

    public function get_data()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            $this->load->library('datatables_ssp');
            $columns = array(
                array('db' => 'npm', 'dt' => 'npm'),
                array('db' => 'nama', 'dt' => 'nama'),
                array('db' => 'prodi', 'dt' => 'prodi'),
                array('db' => 'judul', 'dt' => 'judul'),
                array('db' => 'pembimbing1', 'dt' => 'pembimbing1'), 
                array('db' => 'pembimbing2', 'dt' => 'pembimbing2'),
                array('db' => 'abstrak_judul', 'dt' => 'abstrak_judul'),
                array(
                    'db' => 'npm',
                    'dt' => 'tindakan',
                    'formatter' => function($npm) {
                        return '
                        <a class="btn btn-success btn-sm mb" title="Lihat Data" href="'.site_url('cari_skripsi/view/'.$npm).'">Lihat</a>';
                    }
                ),
            );

            $sql_details = [
                'user' => $this->db->username,
                'pass' => $this->db->password,
                'db' => $this->db->database,
                'host' => $this->db->hostname
            ];


            echo json_encode(
                Datatables_ssp::complex($_GET, $sql_details, self::$table, self::$primaryKey, $columns, NULL, " s_status = 'Selesai' AND s_is_active = 'Aktif' AND s_is_deleted = 'FALSE'")
            );
        }
    }

    public function view()
    {
        $npm = $this->uri->segment(3);

        $where = "npm = '$npm'";

        $data['student'] = $this->m_students->get_students($where);
        $data['title'] = $data['student']['nama']." &minus; SISTEM INFORMASI TUGAS AKHIR - FASILKOM UNSIKA";
        $data['attachment'] = 'Lampiran';
        $data['content'] = 'dashboard/cari_skripsi-view';
        if (!$npm) {
            redirect(site_url('cari_skripsi'));
        } else {
            $this->load->view('dashboard/index', $data);
        }
    }
    
}
