<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Data_skripsi extends CI_Controller {

 	public function __construct() 
    {
        parent::__construct();
        $this->load->model('m_students');
        $this->auth->restrict();
    }

    private static $title = "Skripsi Mahasiswa &minus; SISTEM INFORMASI TUGAS AKHIR - FASILKOM UNSIKA";
    private static $table = 'students';
    private static $primaryKey = 'npm';

    public function index()
	{
        
        $npm = $this->session->userdata['u_name'];
        $where = "npm = '$npm'";
        $data['student'] = $this->m_students->get_students($where);
        $data['attachment'] = 'Lampiran';

        $data['title'] = "Data ".self::$title;
        $data['content'] = "dashboard/data_skripsi";
        $this->load->view('dashboard/index', $data);

    }

    private function validation()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('npm', 'NPM', 'trim|required|max_length[20]|xss_clean');
        $this->form_validation->set_rules('nama', 'Nama Mahasiswa', 'trim|required|min_length[3]|max_length[50]|xss_clean');
        $this->form_validation->set_rules('pob', 'Tempat Lahir', 'trim|required|xss_clean');
        $this->form_validation->set_rules('dob', 'Tanggal Lahir', 'trim|required|xss_clean');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'trim|required|xss_clean');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required|max_length[150]|xss_clean');
        $this->form_validation->set_rules('prodi', 'Program Studi', 'trim|required|max_length[20]|xss_clean');
        $this->form_validation->set_rules('no_hp', 'Nomor Handphone', 'trim|required|max_length[13]|xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[50]xss_clean');
        return $this->form_validation->run();
    }

    public function editskripsi()
    {
        $this->load->helper(['form', 'notification']);
        //$this->session->userdata['u_name'],
        $npm = $this->uri->segment(3);
        $where = "npm = '$npm'";
        $data['student'] = $this->m_students->get_students($where);

        if ($this->validation()) {

            $foto = $_FILES['foto']['name'];

            $this->load->library('upload');

            if (!empty($foto)) {
                $config['upload_path'] = './uploads/foto/';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_ext_tolower'] = TRUE;
                $config['max_size'] = '1024';
                $config['overwrite'] = TRUE;
                $x = explode(".", $foto);
                $ext = strtolower(end($x));
                $config['file_name'] = $npm."-foto.".$ext;
                $foto = $config['file_name'];
                $this->upload->initialize($config);
                $this->upload->do_upload('foto');
            }


            if (!empty($foto) && !$this->upload->do_upload('foto')) {
                $data['err_foto'] = $this->upload->display_errors();
                $where = "m_is_deleted = 'False'";
                $data['majors'] = $this->m_students->get_majors($where);
                $data['title'] = "Tambah ".self::$title;
                $data['form_title'] = "Tambah Mahasiswa";
                $data['action'] = site_url(uri_string());
                $data['content'] = 'dashboard/data_skripsi-edit';
                $this->load->view('dashboard/index', $data);

            } 

            else {

                $data = [
                    'npm' => $npm,
                    'npm' => $this->input->post('npm', TRUE),
                    'nama' => $this->input->post('nama', TRUE),
                    'pob' => $this->input->post('pob', TRUE),
                    'dob' => $this->input->post('dob', TRUE),
                    'jenis_kelamin' => $this->input->post('jenis_kelamin', TRUE),
                    'alamat' => $this->input->post('alamat', TRUE),
                    'prodi' => $this->input->post('prodi', TRUE),
                    'no_hp' => $this->input->post('no_hp', TRUE),
                    'email' => $this->input->post('email', TRUE),
                    'foto' => (!empty($foto)) ? $foto : $data['student']['foto'],
                    's_updated_at' => date('Y-m-d H:i:s'),
                    's_updated_by' => $this->session->userdata['u_name']
                ];

                $this->m_students->editskripsi($data, $npm);
                $this->session->set_flashdata('alert', success('Data berhasil disimpan.'));
                $data['title'] = "Data ".self::$title;
                $data['content'] = "dashboard/data_skripsi";
                redirect(site_url('data_skripsi'));
            }
        } 
        else {
            $data['majors'] = $this->m_students->get_majors();
            $data['title'] = "Edit ".self::$title;
            $data['form_title'] = "Edit Data ".$data['student']['nama'] ;
            $data['action'] = site_url(uri_string());
            $data['content'] = 'dashboard/data_skripsi-edit';
            if (!$npm) {
                redirect(site_url('data_skripsi'));
            } else {
                $this->load->view('dashboard/index', $data);
            }
        }
    }

    public function print_data()
    {
        $npm = $this->uri->segment(3);

        $where = "npm = '$npm'";

        $mhs['student'] = $this->m_students->get_students($where);
        $mhs['title'] = $mhs['student']['nama']." &minus; SISTEM INFORMASI TUGAS AKHIR - FASILKOM UNSIKA";
        $mhs['attachment'] = 'Lampiran';
        if (!$npm) {
            redirect(site_url('data_skripsi'));
        } else {
            $this->load->view('dashboard/data_skripsi-print', $mhs);
        }
    }

    public function get_data()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            if ($this->session->userdata['u_level'] == "Dosen") {
                $this->load->library('datatables_ssp');
                $columns = array(
                    array('db' => 'npm', 'dt' => 'npm'),
                    array('db' => 'nama', 'dt' => 'nama'),
                    array('db' => 'prodi', 'dt' => 'prodi'),
                    array('db' => 'judul', 'dt' => 'judul'),
                    array('db' => 'pembimbing1', 'dt' => 'pembimbing1'),
                    array('db' => 'pembimbing2', 'dt' => 'pembimbing2'),
                    array('db' => 'abstrak_judul', 'dt' => 'abstrak_judul'),
                    array(
                        'db' => 'npm',
                        'dt' => 'tindakan',
                        'formatter' => function($npm) {
                            return '<a class="btn btn-success btn-sm mb" title="Lihat Data" href="'.site_url('data_skripsi/view/'.$npm).'">Lihat</a>';
                        }
                    ),
                );
            } else {
                $this->load->library('datatables_ssp');
                $columns = array(
                    array('db' => 'npm', 'dt' => 'npm'),
                    array('db' => 'nama', 'dt' => 'nama'),
                    array('db' => 'prodi', 'dt' => 'prodi'),
                    array('db' => 'judul', 'dt' => 'judul'),
                    array('db' => 'pembimbing1', 'dt' => 'pembimbing1'),
                    array('db' => 'pembimbing2', 'dt' => 'pembimbing2'),
                    array('db' => 'abstrak_judul', 'dt' => 'abstrak_judul'),
                    array(
                        'db' => 'npm',
                        'dt' => 'tindakan',
                        'formatter' => function($npm) {
                            return '<a class="btn btn-warning btn-sm mb" href="'.site_url('data_skripsi/print_data/'.$npm).'" target="_blank" title="Cetak data"><span class="glyphicon glyphicon-print" aria-hidden="true"></span></a>
                            <a class="btn btn-success btn-sm mb" title="Lihat Data" href="'.site_url('data_skripsi/view/'.$npm).'">Lihat</a>
                            <a class="btn btn-info btn-sm mb" title="Edit Data" href="'.site_url('data_skripsi/edit/'.$npm).'">Edit</a>
                            <a class="btn btn-danger btn-sm mb" onclick="return confirmDialog();" href="'.site_url('data_skripsi/delete/'.$npm).'" title="Hapus Data"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                            <a class="btn btn-default btn-sm mb" title="Ubah Status Mahasiswa" href="'.site_url('data_skripsi/status/'.$npm).'" onclick="return confirmDialogStatus();">Ubah Status</a>';
                        }
                    ),
                );
            }

            $sql_details = [
                'user' => $this->db->username,
                'pass' => $this->db->password,
                'db' => $this->db->database,
                'host' => $this->db->hostname
            ];


            echo json_encode(
                Datatables_ssp::complex($_GET, $sql_details, self::$table, self::$primaryKey, $columns, NULL, "s_is_active = 'Aktif' AND s_is_deleted = 'FALSE'")
            );
        }
    }

    public function view()
    {
        $npm = $this->uri->segment(3);

        $where = "npm = '$npm'";

        $data['student'] = $this->m_students->get_students($where);
        $data['title'] = $data['student']['nama']." &minus; SISTEM INFORMASI TUGAS AKHIR - FASILKOM UNSIKA";
        $data['attachment'] = 'Lampiran';
        $data['content'] = 'dashboard/data_skripsi-view';
        if (!$npm) {
            redirect(site_url('data_skripsi'));
        } else {
            $this->load->view('dashboard/index', $data);
        }
    }

    public function status($npm)
    {
        $this->auth->not_admin();
        $this->load->helper('notification');

        $data = [
            's_updated_at' => date('Y-m-d H:i:s'),
            's_updated_by' => $this->session->userdata['u_name'],
            's_publis' => 'Publis'
        ];

        $this->m_students->statuspublis($data, $npm);
        $this->session->set_flashdata('alert', success('Status mahasiswa berhasil diperbarui.'));
        $data['title'] = "Data ".self::$title;
        $data['content'] = "dashboard/data_skripsi";
        $this->load->view('dashboard/index', $data);
        redirect(site_url('data_skripsi'));
    }
    
    public function delete($npm)
    {
        $this->load->helper('notification');

        $data = [
            's_deleted_at' => date('Y-m-d H:i:s'),
            's_deleted_by' => $this->session->userdata['u_name'],
            's_is_deleted' => TRUE
        ];

        $this->m_students->delete($data, $npm);
        $this->session->set_flashdata('alert', success('Data mahasiswa berhasil dihapus.'));
        $data['title'] = "Data ".self::$title;
        $data['content'] = "dashboard/data_skripsi";
        $this->load->view('dashboard/index', $data);
        redirect(site_url('data_skripsi'));
    }

    public function deleted()
    {
        $data['title'] = "SISTEM INFORMASI TUGAS AKHIR - FASILKOM UNSIKA";
        $data['content'] = "dashboard/data_skripsi-deleted";
        $this->load->view('dashboard/index', $data);
    }

    public function get_deleted()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            $this->load->library('datatables_ssp');
            $columns = array(
                array('db' => 'npm', 'dt' => 'npm'),
                array('db' => 'nama', 'dt' => 'nama'),
                array('db' => 'pob', 'dt' => 'pob'),
                array('db' => 'dob', 'dt' => 'dob'),
                array('db' => 'jenis_kelamin', 'dt' => 'jenis_kelamin'),
                array('db' => 'alamat', 'dt' => 'alamat'),
                array('db' => 'prodi', 'dt' => 'prodi'),
                array('db' => 'no_hp', 'dt' => 'no_hp'),
                array('db' => 'email', 'dt' => 'email'),
                array(
                    'db' => 'npm',
                    'dt' => 'tindakan',
                    'formatter' => function($npm) {
                        return '<a class="btn btn-success btn-sm" onclick="return confirmDialog();" href="'.site_url('data_skripsi/restore/'.$npm).'"><span class="glyphicon glyphicon-repeat" aria-hidden="true"></span> Restore</a>
                        <a class="btn btn-danger btn-sm" onclick="return confirmDialogDelete();" href="'.site_url('data_skripsi/deletex/'.$npm).'"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Hapus</a>';
                    }
                ),
            );

            $sql_details = [
                'user' => $this->db->username,
                'pass' => $this->db->password,
                'db' => $this->db->database,
                'host' => $this->db->hostname
            ];


            echo json_encode(
                Datatables_ssp::complex($_GET, $sql_details, self::$table, self::$primaryKey, $columns, NULL, "s_is_active = 'Aktif' AND s_is_deleted = 'TRUE'")
            );
        }
    }

    public function restore()
    {
        $this->load->helper(['form', 'notification']);
        $npm = $this->uri->segment(3);

        $data = [
            's_restored_at' => date('Y-m-d H:i:s'),
            's_restored_by' => $this->session->userdata['u_npm'],
            's_is_deleted' => 'FALSE'
        ];

        $this->m_students->restore($data ,$npm);
        $this->session->set_flashdata('alert', success('Data berhasil direstore.'));
        $data['title'] = "Data ".self::$title;
        $data['content'] = "dashboard/data_skripsi-deleted";
        redirect(site_url('data_skripsi/deleted'));
    }

    public function deletex($npm)
    {
        $sql = $this->m_students->deletex($npm);
        if($sql == "success"){
            redirect(site_url('data_skripsi/deleted'));
        }else{
            redirect(site_url('data_skripsi/deleted'));
        }
    }


}
