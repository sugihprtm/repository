<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

 	public function __construct()
    {
        parent::__construct();
        $this->auth->restrict();
        $this->load->model('m_dashboards'); 
}


	public function index()
	{
        // total mahasiswa aktif 
        $ma = "s_is_deleted = 'false' AND s_is_active = 'Aktif'";
        $data['total'] = count($this->m_dashboards->total($ma));

        // mahasiswa selesai
        $ms = "s_is_deleted = 'false' AND s_is_active = 'Aktif' AND s_status = 'Selesai'";
        $selesai = $this->m_dashboards->totalms($ms);
        $data['totalms'] = count($selesai);

        if ($data['total'] < 1) {
            $data['persenms'] = 0;
        } else {
            $data['persenms'] = substr(($data['totalms'] / $data['total'] * 100), 0, 5);
        }

        // TA Belum Publis
        $wb = "s_publis = 'Belum' AND s_is_deleted = 'false' AND s_is_active = 'Aktif'";
        $belum = $this->m_dashboards->belum($wb);
        $data['totalb'] = count($belum);

        if ($data['totalb'] < 1) {
            $data['persenb'] = 0;
        } else {
            $data['persenb'] = substr(($data['totalb'] / $data['total'] * 100), 0, 5);
        }


        // TA publis
        $wl = "s_publis = 'Publis' AND s_is_deleted = 'false' AND s_is_active = 'Aktif'";
        $lengkap = $this->m_dashboards->lengkap($wl);
        $data['totall'] = count($lengkap);

        if ($data['totall'] < 1) {
            $data['persenl'] = 0;
        } else {
            $data['persenl'] = substr(($data['totall'] / $data['total'] * 100), 0, 5);
        }

        $data['title'] = "SISTEM INFORMASI TUGAS AKHIR - FASILKOM UNSIKA";
        $data['content'] = 'dashboard/home';
		$this->load->view('dashboard/index', $data);
	}
}
