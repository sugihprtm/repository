<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_users extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	private static $table = 'users';
	private static $table2 = 'students';
	private static $pk = 'u_name'; 

	public function is_exist($where)
	{
		return $this->db->where($where)->get(self::$table)->row_array();
	}

	public function add($data)
	{
    	return $this->db->insert(self::$table, $data);
	}

	public function add2($data2)
	{
    	return $this->db->insert(self::$table2, $data2);
	}

	public function get_user($where)
	{
		return $this->db->where($where)->get(self::$table)->row_array();
	}

	public function edit($data, $u_name)
	{
		return $this->db->set($data)->where(self::$pk, $u_name)->update(self::$table);
	}

	public function status($data2, $npm)
	{
		return $this->db->set($data2)->where(self::$pk, $npm)->update(self::$table);
	}

	public function reset($data, $u_name)
	{
		return $this->db->set($data)->where(self::$pk, $u_name)->update(self::$table);
	}

	public function update($data, $u_name)
	{
		return $this->db->set($data)->where(self::$pk, $u_name)->update(self::$table);
	}

	public function deletex($u_name)
	{
		$query = $this->db->query("delete from users where u_name = '$u_name'");
		$query = $this->db->query("delete from students where npm = '$u_name'");
		if ($query) {
			return "success";
		} else {
			return "failed";
		}
	}
}
