<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_students extends CI_Model { 

	public function __construct()
	{
		parent::__construct();
	}

	private static $table = 'students';
	private static $pk = 'npm';

	public function is_exist($where)
	{
		return $this->db->where($where)->get(self::$table)->row_array();
	}

	public function get_students($where)
	{
		$query = $this 
					->db
					->select('*')
					->from(self::$table)
					->where($where)
					->get();

		if ($query->num_rows() > 0) {
			return $query->row_array();
		} else {
			return NULL;
		}
	}

	public function get_deleted($where)
	{
		$query = $this
					->db
					->select('*')
					->from(self::$table)
					->where($where)
					->get();

		if ($query->num_rows() > 0) {
			return $query->row_array();
		} else {
			return NULL;
		}
	}

	public function get_majors()
	{
		return $this->db->get('majors')->result_array();
	}

	public function get_dosen()
	{
		return $this->db->get('dosen')->result_array();
	}

	public function add($data)
	{
    	return $this->db->insert(self::$table, $data);
	}

	public function do_import($data)
	{
        return $this->db->insert(self::$table, $data);
	}

	public function edit($data, $npm)
	{
		return $this->db->set($data)->where(self::$pk, $npm)->update(self::$table);
	}

	public function editskripsi($data, $npm)
	{
		return $this->db->set($data)->where(self::$pk, $npm)->update(self::$table);
	}

	public function status($data, $npm)
	{
		return $this->db->set($data)->where(self::$pk, $npm)->update(self::$table);
	}

	public function statuspublis($data, $npm)
	{
		return $this->db->set($data)->where(self::$pk, $npm)->update(self::$table); 
	}

	public function active($data, $npm)
	{
		return $this->db->set($data)->where(self::$pk, $npm)->update(self::$table);
	}

	public function delete($data, $npm)
	{
		return $this->db->set($data)->where(self::$pk, $npm)->update(self::$table);
	}

	public function restore($data, $npm)
	{
		return $this->db->set($data)->where(self::$pk, $npm)->update(self::$table);
	}
	
	public function deletex($npm)
	{
		$query = $this->db->query("delete from students where npm = '$npm'");
		if ($query) {
			return "success";
		} else {
			return "failed";
		}
	}

}
