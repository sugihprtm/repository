<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_homes extends CI_Model { 

	public function __construct()
	{
		parent::__construct();
	}

	private static $table = 'students';
	

	public function get_result($keyword)
	{
		$where = "s_publis = 'Publis'";
		$query = $this
					->db
					->select('*')
	                ->from(self::$table)
	                ->like('judul', $keyword)
	                ->where($where)
					->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
	}

	public function get_data($npm)
	{
		$where = "npm = '$npm'";

		$query = $this
					->db
					->select('*')
					->from(self::$table)
					->where($where)
					->get();

		if ($query->num_rows() > 0) {
			return $query->row_array();
		} else {
			return NULL;
		}
	}
}
