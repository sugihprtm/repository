<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_dosen extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	private static $table = 'dosen';
	private static $pk = 'nidn';
 
	public function is_exist($where)
	{
		return $this->db->where($where)->get(self::$table)->row_array();
	}

	public function add($data)
	{
    	return $this->db->insert(self::$table, $data);
	}

	public function get_dosen($where)
	{
		return $this->db->where($where)->get(self::$table)->row_array();
	}

	public function edit($data, $nidn)
	{
		return $this->db->set($data)->where(self::$pk, $nidn)->update(self::$table);
	}

	public function delete($data, $nidn)
	{
		return $this->db->set($data)->where(self::$pk, $nidn)->update(self::$table);
	}

	public function restore($data, $nidn)
	{
		return $this->db->set($data)->where(self::$pk, $nidn)->update(self::$table);
	}

	public function deletex($nidn)
	{
		$query = $this->db->query("delete from majors where nidn = '$nidn'");
		if ($query) {
			return "success";
		} else {
			return "failed";
		}
	}
}
