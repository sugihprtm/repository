<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h2>Data Skripsi Fasilkom</h2>
<div class="table-responsive"> 
    <table id="cari_skripsi" class="display table table-bordered table-hover table-responsive">
        <thead>
            <tr>
                <th width="5%">No</th>
                <th width="8%">NPM</th>
                <th width="15%">Nama</th>
                <th width="10%">Prodi</th>
                <th width="32%">Judul</th>
                <th width="10%">Pembimbing 1</th>
                <th width="10%">Pembimbing 2</th>
                <th width="10%">Tindakan</th>
            </tr>
        </thead>
    </table>
</div>
