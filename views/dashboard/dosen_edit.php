<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h2><?=$form_title?></h2>
<hr>
<?=form_open($action, 'class="form-horizontal"')?>
    <div class="form-group">
        <label class="col-sm-2 control-label">NIDN Dosen</label>
        <div class="col-sm-6">
            <input type="text" name="nidn" class="form-control" value="<?=(set_value('nama')) ? set_value('nama') : $dosen['nama']?>" placeholder="NIDN Dosen" minlength="10" maxlength="50" required>
            <small class="text-danger"><?=form_error('nama')?></small>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Nama Dosen</label>
        <div class="col-sm-3">
            <input type="text" name="nama" class="form-control" value="<?=(set_value('nidn')) ? set_value('nidn') : $dosen['nidn']?>" placeholder="Nama Lengkap Dosen" maxlength="50" required>
            <small class="text-danger"><?=form_error('nidn')?></small>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" name="submit" class="btn btn-success">Simpan</button>&nbsp;
            <a class="btn btn-default" href="<?=site_url('dosen')?>">Kembali</a>
        </div>
    </div>
<?=form_close()?>
