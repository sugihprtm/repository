<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h2>Data Mahasiswa<a class="btn btn-success btn-sm add" href="<?=site_url('data_mahasiswa/import')?>">Import Data</a></h2>
<div class="table-responsive">
    <table id="mahasiswa-deleted" class="display table table-bordered table-hover table-responsive">
        <thead>
            <tr>
                <th width="5%">No</th>
                <th width="10%">NPM</th>
                <th width="20%">Nama</th>
                <th width="10%">Tempat Lahir</th>
                <th width="10%">Tanggal Lahir</th>
                <th width="10%">Jenis Kelamin</th>
                <th width="15%">Alamat</th>
                <th width="10%">Prodi</th>
                <th width="10%">No HP</th>
                <th width="10%">Email</th>
                <th width="10%">Tindakan</th>
            </tr>
        </thead>
    </table>
    <script>
        function confirmDialog() {
            return confirm("Apakah Anda yakin akan merestore data ini?")
        }

        function confirmDialogDelete() {
            return confirm("Apakah Anda yakin akan menghapus data ini?")
        }
    </script>
</div>
