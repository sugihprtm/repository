<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper">
    <div class="header"> 
        <div class="logo">
            <img src="<?=site_url('assets/img/fasilkom.png')?>" alt="Logo">
        </div>
        <h1>Repository Tugas Akhir</h1>
        <h3>Fakultas Ilmu Komputer, Universitas Singaperbangsa Karawang</h3>
        <p>Website ini berisi informasi tugas akhir mahasiswa Fakultas Ilmu Komputer, Universitas Singaperbangsa Karawang.</p>
    </div>
    <div class="center">
        <?=form_open($action, 'class="form-inline"')?>
            <div class="form-group">
                <div class="input-group">
                <div class="input-group-addon md"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></div>
                    <input type="text" class="form-control md search" id="search_skripsi" placeholder="NPM / Nama / Judul / Kata Kunci / Tahun" autocomplete="off" required>
                </div>
            </div> 
            <div id="hint">
                <p class="help-block">Masukkan NPM / Nama / Judul / Kata Kunci / Tahun dan hasil akan otomatis ditampilkan disini.<br>
                <small>Contoh format tanggal 1998-11-20.</small></p>
            </div>
        <?=form_close()?>
    </div>
</div>
