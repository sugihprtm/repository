<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!doctype html>
<html lang="id">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" href="<?=site_url('assets/img/logo.png')?>">
        <meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1">
        <title><?=$title?></title>
        <?=link_tag('assets/css/bootstrap.css?ver=3.3.7')?>
        <?=link_tag('assets/css/style.css?ver=1.0.0')?>
    </head>
    <body onload="window.print()">
        <div class="container isi">
            <div class="row print">
                <div class="col-sm-3" id="foto">
                    <?php if (!empty($student['foto'])) { ?>
                        <img class="foto" src="<?=site_url('uploads/foto/'.$student['foto'])?>" alt="Foto">
                    <?php } else { ?>
                        <img class="foto" src="<?=site_url('assets/img/avatar.png')?>" alt="Foto">
                    <?php } ?>
                </div>
                <div class="col-sm-9" id="data">
                    <table class="table siswa">
                        <tbody>
                <tr>
                    <td width="150px">NPM</td>
                    <td width="5px">:</td>
                    <td><?=$student['npm']?></td>
                </tr>
                <tr>
                    <td width="150px">Nama</td>
                    <td width="5px">:</td>
                    <td><?=$student['nama']?></td>
                </tr>
                 <tr>
                    <td width="150px">Tempat Lahir</td>
                    <td width="5px">:</td>
                    <td><?=$student['pob']?></td>
                </tr>
                <tr>
                    <td width="150px">Tanggal Lahir</td>
                    <td width="5px">:</td>
                    <td><?=$student['dob']?></td>
                </tr>
                <tr>
                    <td width="150px">Jenis Kelamin</td>
                    <td width="5px">:</td>
                    <td><?=$student['jenis_kelamin']?></td>
                </tr>
                <tr>
                    <td width="150px">Alamat</td>
                    <td width="5px">:</td>
                    <td><?=$student['alamat']?></td>
                </tr>
                <tr>
                    <td width="150px">Program Studi</td>
                    <td width="5px">:</td>
                    <td><?=$student['prodi']?></td>
                </tr>
                <tr>
                    <td width="150px">No Handphone </td>
                    <td width="5px">:</td>
                    <td><?=$student['no_hp']?></td>
                </tr>
                <tr>
                    <td width="150px">Email</td>
                    <td width="5px">:</td>
                    <td><?=$student['email']?></td>
                </tr>

                <tr>
                    <td width="150px">Status</td>
                    <td width="5px">:</td>
                    <td>
                        <?php if ($student['s_status'] == "Lengkap") { ?>
                            <span class="btn btn-success btn-sm">Selesai</span>
                        <?php } else if ($student['s_status'] == "Kurang") { ?>
                            <span class="btn btn-warning btn-sm kurang">Selesai</span>
                        <?php } else { ?>
                            <span class="btn btn-danger btn-sm belum">Belum Selesai</span>
                        <?php } ?>
                    </td>
                </tr>
            </tbody>
                    </table>
                </div>
            </div>
            
        </div>
    </body>
</html>
