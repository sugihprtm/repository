<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h2>Data Mahasiswa</h2>
<div class="table-responsive">
    <table id="mahasiswa" class="display table table-bordered table-hover table-responsive">
        <thead>
            <tr>
                <th width="5%">No</th>
                <th width="10%">NPM</th>
                <th width="20%">Nama</th>
                <th width="10%">Tempat Lahir</th>
                <th width="10%">Tanggal Lahir</th>
                <th width="10%">Jenis Kelamin</th>
                <th width="15%">Alamat</th>
                <th width="10%">Prodi</th>
                <th width="10%">No HP</th>
                <th width="10%">Email</th>
                <th width="10%">Tindakan</th>
            </tr>
        </thead>
    </table>
    <script>
        function confirmDialog() {
            return confirm("Apakah Anda yakin akan menghapus data ini?")
        }

        function confirmDialogStatus() {
            <?php if ($this->session->userdata['u_level'] == "Administrator") { ?>
                return confirm("Apakah Anda yakin akan mengubah status skripsi mahasiswa ini menjadi selesai?")
            <?php } else { ?>
                window.alert("Hanya Administrator yang boleh mengubah status mahasiswa!")
            <?php } ?>
        }
    </script>
</div>
