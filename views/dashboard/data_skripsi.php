<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h2>Data Tugas Akhir Mahasiswa</h2>
<div class="table-responsive"> 
    <table id="skripsi" class="display table table-bordered table-hover table-responsive">
        <thead>
            <tr>
                <th width="5%">No</th>
                <th width="10%">NPM</th>
                <th width="20%">Nama</th>
                <th width="10%">Prodi</th>
                <th width="10%">Judul</th>
                <th width="10%">Pembimbing 1</th>
                <th width="10%">Pembimbing 2</th>
                <th width="10%">Tindakan</th>
            </tr>
        </thead>
    </table>
    <script>
        function confirmDialog() {
            return confirm("Apakah Anda yakin akan menghapus data ini?")
        }

        function confirmDialogStatus() {
            <?php if ($this->session->userdata['u_level'] == "Administrator") { ?>
                return confirm("Apakah Anda yakin akan mengubah status skripsi mahasiswa ini menjadi publis?")
            <?php } else { ?>
                window.alert("Hanya Administrator yang boleh mengubah status skripsi mahasiswa!")
            <?php } ?>
        }
    </script>
</div>
