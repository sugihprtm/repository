<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h2><?=$form_title?></h2>
<hr>
<?=form_open_multipart($action, 'class="form-horizontal"')?>
<h3>Data Mahasiswa</h3>
<hr>
    <div class="form-group">
        <label class="col-sm-2 control-label">NPM</label>
        <div class="col-sm-5">
            <input type="number" name="npm" class="form-control" value="<?=isset($student['npm']) ? $student['npm'] : set_value('npm')?>" placeholder="NPM" required>
            <small class="text-danger"><?=form_error('npm')?></small>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Nama</label>
        <div class="col-sm-5">
            <input type="text" name="nama" class="form-control" value="<?=isset($student['nama']) ? $student['nama'] : set_value('nama')?>" placeholder="Nama" required>
            <small class="text-danger"><?=form_error('nama')?></small>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Tempat Lahir</label>
        <div class="col-sm-5">
            <input type="text" name="pob" class="form-control" value="<?=isset($student['pob']) ? $student['pob'] : set_value('pob')?>" placeholder="Tempat Lahir" required>
            <small class="text-danger"><?=form_error('pob')?></small>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Tanggal Lahir</label>
        <div class="col-sm-5">
            <input type="date" name="dob" class="form-control" value="<?=isset($student['dob']) ? $student['dob'] : set_value('dob')?>" placeholder="Tanggal Lahir" required>
            <small class="text-danger"><?=form_error('dob')?></small>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Jenis Kelamin</label>
        <div class="col-sm-5">
            <select name="jenis_kelamin" class="form-control" required>
                <option value="<?=isset($student['jenis_kelamin']) ? $student['jenis_kelamin'] : set_value('jenis_kelamin')?>"><?=isset($student['jenis_kelamin']) ? $student['jenis_kelamin'] : set_value('jenis_kelamin')?></option>
                <option value="Laki-laki">Laki-laki</option>
                <option value="Perempuan">Perempuan</option>
            </select>
            <small class="text-danger"><?=form_error('jenis_kelamin')?></small>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Alamat</label>
        <div class="col-sm-5">
            <textarea class="form-control" rows="5" name="alamat" required> <?=isset($student['alamat']) ? $student['alamat'] : set_value('alamat')?> </textarea>
            <small class="text-danger"><?=form_error('alamat')?></small>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Program Studi</label>
        <div class="col-sm-5">
            <select name="prodi" class="form-control" required>
                <option value="<?=isset($student['prodi']) ? $student['prodi'] : set_value('prodi')?>"><?=isset($student['prodi']) ? $student['prodi'] : set_value('prodi')?></option>
                <?php foreach (array_reverse($majors) as $row) {     ?>
                    <option value="<?=$row['prodi']?>"><?=$row['prodi']?></option>
                <?php } ?>
            </select>
            <small class="text-danger"><?=form_error('prodi')?></small>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Nomor Handphone</label>
        <div class="col-sm-5">
            <input type="text" name="no_hp" class="form-control" value="<?=isset($student['no_hp']) ? $student['no_hp'] : set_value('no_hp')?>" placeholder="Tanggal Lahir" required>
            <small class="text-danger"><?=form_error('no_hp')?></small>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Email</label>
        <div class="col-sm-5">
            <input type="email" name="email" class="form-control" value="<?=isset($student['email']) ? $student['email'] : set_value('email')?>" placeholder="Tanggal Lahir" required>
            <small class="text-danger"><?=form_error('email')?></small>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Foto</label>
        <div class="col-sm-10">
            <?php if ($this->uri->segment(2) == 'edit') { ?>
                <?php if (file_exists('./uploads/foto/' . $student['foto'])) { ?>
                    <a href="<?=site_url('uploads/foto/' . $student['foto'])?>" target="_blank" ><?=$student['foto']?></a>
                <?php } ?>
                <input type="file" name="foto">
                <small class="help-block">Format gambar yang diperbolehkan *.png, *.jpg dan ukuran maksimal 1 MB.</small>
                <small class="text-danger"><?=!empty($err_foto) ? $err_foto : "";?></small>
            <?php } else { ?>
                <input type="file" name="foto">
                <small class="help-block">Format gambar yang diperbolehkan *.png, *.jpg dan ukuran maksimal 1 MB.</small>
                <small class="text-danger"><?=!empty($err_foto) ? $err_foto : "";?></small>
            <?php } ?>
        </div>
    </div>

    <hr>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" name="submit" class="btn btn-success" id="submit">Simpan</button>&nbsp;
            <a class="btn btn-default" href="<?=site_url('profil')?>">Kembali</a>
        </div>
    </div>
<?=form_close()?>
