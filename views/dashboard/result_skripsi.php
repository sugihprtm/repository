<?php defined('BASEPATH') OR exit('No direct script access allowed');

if(!empty($result)) {

	$output = '';
	$outputdata = '';
	$outputtail = '';
	$output .= '<div class="left-text">
				<div class="table">
				<table class="table table-hover">
					<thead>
						<tr>
							<th width="10%">NPM</th>
							<th width="20%">Nama</th>
							<th width="70%">Judul</th>
						</tr>
				   </thead>
				   <tbody>';

				echo '<h4 class="left-text">'.count($result).' Data ditemukan untuk "<u>'.$keyword.'</u>"</h4>';

				foreach ($result as $row) {

					$outputdata .= '
						<tr>
							<td>'.$row->npm.'</td>
							<td>'.$row->nama.'</td>
							<td>'.$row->judul.'</td>
						</tr>';
			 	}

				$outputtail .= '
		 		</tbody>
			</table>
		</div>
	</div> ';

 	echo $output;
	echo $outputdata;
	echo $outputtail;
} else {
  	echo '<div class="err_notif"><h3 class="text-danger"><span><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> Tidak ada data yang ditemukan untuk "<u>'.$keyword.'</u>"</span></div>';
}
