<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h2><?=$form_title?></h2>
<hr>
<?=form_open_multipart($action, 'class="form-horizontal"')?>
<h3>Data Mahasiswa</h3>
<hr>
    <div class="form-group">
        <label class="col-sm-2 control-label">NPM</label>
        <div class="col-sm-5">
            <input type="number" name="npm" class="form-control" value="<?=isset($student['npm']) ? $student['npm'] : set_value('npm')?>" placeholder="NPM" readonly>
            <small class="text-danger"><?=form_error('npm')?></small>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Nama</label>
        <div class="col-sm-5">
            <input type="text" name="nama" class="form-control" value="<?=isset($student['nama']) ? $student['nama'] : set_value('nama')?>" placeholder="Nama" readonly>
            <small class="text-danger"><?=form_error('nama')?></small>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Program Studi</label>
        <div class="col-sm-5">
            <input type="text" name="prodi" class="form-control" value="<?=isset($student['prodi']) ? $student['prodi'] : set_value('prodi')?>" placeholder="Tanggal Lahir" readonly>
            <small class="text-danger"><?=form_error('prodi')?></small>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Judul</label>
        <div class="col-sm-5">
            <input type="text" name="judul" class="form-control" value="<?=isset($student['judul']) ? $student['judul'] : set_value('judul')?>" placeholder="Judul Skripsi" required>
            <small class="text-danger"><?=form_error('judul')?></small>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Pembimbing 1</label>
        <div class="col-sm-5">
            <select name="pembimbing1" class="form-control" required>
                <option value="<?=isset($student['pembimbing1']) ? $student['pembimbing1'] : set_value('pembimbing1')?>"><?=isset($student['pembimbing1']) ? $student['pembimbing1'] : set_value('pembimbing1')?></option>
                <?php foreach (array_reverse($dosen) as $row) {     ?>
                    <option value="<?=$row['nama']?>"><?=$row['nama']?></option>
                <?php } ?>
            </select>
            <small class="text-danger"><?=form_error('pembimbing1')?></small>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Pembimbing 2</label>
        <div class="col-sm-5">
            <select name="pembimbing2" class="form-control" required>
                <option value="<?=isset($student['pembimbing2']) ? $student['pembimbing2'] : set_value('pembimbing2')?>"><?=isset($student['pembimbing2']) ? $student['pembimbing2'] : set_value('pembimbing2')?></option>
                <?php foreach (array_reverse($dosen) as $row) {     ?>
                    <option value="<?=$row['nama']?>"><?=$row['nama']?></option>
                <?php } ?>
            </select>
            <small class="text-danger"><?=form_error('pembimbing2')?></small>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Abstrak</label>
        <div class="col-sm-8">
            <textarea class="form-control" rows="8" name="abstrak_judul" required> <?=isset($student['abstrak_judul']) ? $student['abstrak_judul'] : set_value('abstrak_judul')?> </textarea>
            <small class="text-danger"><?=form_error('abstrak_judul')?></small>
        </div>
    </div>

    <hr>
    <h3>Dokumen Skripsi</h3>
    <p class="help-block">Format yang diperbolehkan *.doc, *.docx, *.pdf, *.zip dan *.rar.</p>
    <p class="help-block">Format nama file yaitu 5 digit npm_nama jenis file. Contoh : 15134_Bab1</p>
    <hr>
    
    <div class="form-group">
        <label class="col-sm-3 control-label">Daftar Isi, Tabel, Gambar</label>
        <div class="col-sm-9">
            <?php if ($this->uri->segment(2) == 'edit') { ?>
                <?php if (file_exists('./uploads/daftarisi/' . $student['daftarisi'])) { ?>
                    <a href="<?=site_url('uploads/daftarisi/' . $student['daftarisi'])?>" target="_blank" ><?=$student['daftarisi']?></a>
                <?php } ?>
                <input type="file" name="daftarisi">
                <small class="text-danger"><?=!empty($err_daftarisi) ? $err_daftarisi: "";?></small>
            <?php } else { ?>
                <input type="file" name="daftarisi">
                <small class="text-danger"><?=!empty($err_daftarisi) ? $err_daftarisi: "";?></small>
            <?php } ?>
        </div> 
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Abstrak</label>
        <div class="col-sm-9">
            <?php if ($this->uri->segment(2) == 'edit') { ?>
                <?php if (file_exists('./uploads/abstrak/' . $student['abstrak'])) { ?>
                    <a href="<?=site_url('uploads/abstrak/' . $student['abstrak'])?>" target="_blank" ><?=$student['abstrak']?></a>
                <?php } ?>
                <input type="file" name="abstrak">
                <small class="text-danger"><?=!empty($err_abstrak) ? $err_abstrak : "";?></small>
            <?php } else { ?>
                <input type="file" name="abstrak">
                <small class="text-danger"><?=!empty($err_abstrak) ? $err_abstrak: "";?></small>
            <?php } ?>
        </div> 
    </div>
    
    <div class="form-group">
        <label class="col-sm-3 control-label">Bab 1</label>
        <div class="col-sm-9">
            <?php if ($this->uri->segment(2) == 'edit') { ?>
                <?php if (file_exists('./uploads/bab1/' . $student['bab1'])) { ?>
                    <a href="<?=site_url('uploads/bab1/' . $student['bab1'])?>" target="_blank" ><?=$student['bab1']?></a>
                <?php } ?>
                <input type="file" name="bab1">
                <small class="text-danger"><?=!empty($err_bab1) ? $err_bab1 : "";?></small>
            <?php } else { ?>
                <input type="file" name="bab1">
                <small class="text-danger"><?=!empty($err_bab1) ? $err_bab1 : "";?></small>
            <?php } ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Bab 2</label>
        <div class="col-sm-9">
            <?php if ($this->uri->segment(2) == 'edit') { ?>
                <?php if (file_exists('./uploads/bab2/' . $student['bab2'])) { ?>
                    <a href="<?=site_url('uploads/bab2/' . $student['bab2'])?>" target="_blank" ><?=$student['bab2']?></a>
                <?php } ?>
                <input type="file" name="bab2">
                <small class="text-danger"><?=!empty($err_bab2) ? $err_bab2: "";?></small>
            <?php } else { ?>
                <input type="file" name="bab2">
                <small class="text-danger"><?=!empty($err_bab2) ? $err_bab2 : "";?></small>
            <?php } ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Bab 3</label>
        <div class="col-sm-9">
            <?php if ($this->uri->segment(2) == 'edit') { ?>
                <?php if (file_exists('./uploads/bab3/' . $student['bab3'])) { ?>
                    <a href="<?=site_url('uploads/bab3/' . $student['bab3'])?>" target="_blank" ><?=$student['bab3']?></a>
                <?php } ?>
                <input type="file" name="bab3">
                <small class="text-danger"><?=!empty($err_bab3) ? $err_bab3 : "";?></small>
            <?php } else { ?>
                <input type="file" name="bab3">
                <small class="text-danger"><?=!empty($err_bab3) ? $err_bab3 : "";?></small>
            <?php } ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Bab 4</label>
        <div class="col-sm-9">
            <?php if ($this->uri->segment(2) == 'edit') { ?>
                <?php if (file_exists('./uploads/bab4/' . $student['bab4'])) { ?>
                    <a href="<?=site_url('uploads/bab4/' . $student['bab4'])?>" target="_blank" ><?=$student['bab4']?></a>
                <?php } ?>
                <input type="file" name="bab4">
                <small class="text-danger"><?=!empty($err_bab4) ? $err_bab4 : "";?></small>
            <?php } else { ?>
                <input type="file" name="bab4">
                <small class="text-danger"><?=!empty($err_bab4) ? $err_bab4 : "";?></small>
            <?php } ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Bab 5</label>
        <div class="col-sm-9">
            <?php if ($this->uri->segment(2) == 'edit') { ?>
                <?php if (file_exists('./uploads/bab5/' . $student['bab5'])) { ?>
                    <a href="<?=site_url('uploads/bab5/' . $student['bab5'])?>" target="_blank" ><?=$student['bab5']?></a>
                <?php } ?>
                <input type="file" name="bab5">
                <small class="text-danger"><?=!empty($err_bab5) ? $err_bab5 : "";?></small>
            <?php } else { ?>
                <input type="file" name="bab5">
                <small class="text-danger"><?=!empty($err_bab5) ? $err_bab5 : "";?></small>
            <?php } ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Daftar Pustaka</label>
        <div class="col-sm-9">
            <?php if ($this->uri->segment(2) == 'edit') { ?>
                <?php if (file_exists('./uploads/daftarpustaka/' . $student['daftarpustaka'])) { ?>
                    <a href="<?=site_url('uploads/daftarpustaka/' . $student['daftarpustaka'])?>" target="_blank" ><?=$student['daftarpustaka']?></a>
                <?php } ?>
                <input type="file" name="daftarpustaka">
                <small class="text-danger"><?=!empty($err_daftarpustaka) ? $err_daftarpustakadaftarpustaka : "";?></small>
            <?php } else { ?>
                <input type="file" name="daftarpustaka">
                <small class="text-danger"><?=!empty($err_daftarpustakadaftarpustaka) ? $err_daftarpustaka : "";?></small>
            <?php } ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Lampiran</label>
        <div class="col-sm-9">
            <?php if ($this->uri->segment(2) == 'edit') { ?>
                <?php if (file_exists('./uploads/lampiran/' . $student['lampiran'])) { ?>
                    <a href="<?=site_url('uploads/lampiran/' . $student['lampiran'])?>" target="_blank" ><?=$student['lampiran']?></a>
                <?php } ?>
                <input type="file" name="lampiran">
                <small class="text-danger"><?=!empty($err_lampiran) ? $err_lampiran : "";?></small>
            <?php } else { ?>
                <input type="file" name="lampiran">
                <small class="text-danger"><?=!empty($err_lampiran) ? $err_lampiran : "";?></small>
            <?php } ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Full Skripsi</label>
        <div class="col-sm-9">
            <?php if ($this->uri->segment(2) == 'edit') { ?>
                <?php if (file_exists('./uploads/fullskripsi/' . $student['fullskripsi'])) { ?>
                    <a href="<?=site_url('uploads/fullskripsi/' . $student['fullskripsi'])?>" target="_blank" ><?=$student['fullskripsi']?></a>
                <?php } ?>
                <input type="file" name="fullskripsi">
                <small class="text-danger"><?=!empty($err_fullskripsi) ? $err_fullskripsin : "";?></small>
            <?php } else { ?>
                <input type="file" name="fullskripsi">
                <small class="text-danger"><?=!empty($err_fullskripsi) ? $err_fullskripsi : "";?></small>
            <?php } ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Source Code</label>
        <div class="col-sm-9">
            <?php if ($this->uri->segment(2) == 'edit') { ?>
                <?php if (file_exists('./uploads/sourcecode/' . $student['sourcecode'])) { ?>
                    <a href="<?=site_url('uploads/sourcecode/' . $student['sourcecode'])?>" target="_blank" ><?=$student['sourcecode']?></a>
                <?php } ?>
                <input type="file" name="sourcecode">
                <small class="text-danger"><?=!empty($err_sourcecode) ? $err_sourcecode : "";?></small>
            <?php } else { ?>
                <input type="file" name="sourcecode">
                <small class="text-danger"><?=!empty($err_sourcecode) ? $err_sourcecode : "";?></small>
            <?php } ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Compile Program</label>
        <div class="col-sm-9">
            <?php if ($this->uri->segment(2) == 'edit') { ?>
                <?php if (file_exists('./uploads/program/' . $student['program'])) { ?>
                    <a href="<?=site_url('uploads/program/' . $student['program'])?>" target="_blank" ><?=$student['program']?></a>
                <?php } ?>
                <input type="file" name="program">
                <small class="text-danger"><?=!empty($err_program) ? $err_program : "";?></small>
            <?php } else { ?>
                <input type="file" name="program">
                <small class="text-danger"><?=!empty($err_program) ? $err_program : "";?></small>
            <?php } ?>
        </div>
    </div>

    <hr>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" name="submit" class="btn btn-success" id="submit">Simpan</button>&nbsp;
            <a class="btn btn-default" href="<?=site_url('upload_skripsi')?>">Kembali</a>
        </div>
    </div>
<?=form_close()?>
