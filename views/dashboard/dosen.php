<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h2>Data Dosen <a class="btn btn-success btn-sm add" href="<?=site_url('dosen/add')?>"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Tambah Data</a></h2>
<div class="table-responsive">
    <table id="dosen" class="display table table-bordered table-hover table-responsive">
        <thead>
            <tr>
                <th width="5%">No</th>
                <th width="27%">NIDN</th>
                <th width="20%">NAMA DOSEN</th>
                <th width="18%">Dibuat Pada</th>
                <th width="18%">Diperbarui Pada</th>
                <th width="12%">Tindakan</th>
            </tr>
        </thead>
    </table>
    <script>
        function confirmDialog() {
            return confirm("Apakah Anda yakin akan menghapus data ini?")
        }
    </script>
</div>
