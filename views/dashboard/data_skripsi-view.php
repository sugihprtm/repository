<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row print"> 
    <div class="col-sm-3" id="foto">
        <?php if (!empty($student['foto'])) { ?>
            <img class="foto" src="<?=site_url('uploads/foto/'.$student['foto'])?>" alt="Foto">
        <?php } else { ?>
            <img class="foto" src="<?=site_url('assets/img/avatar.png')?>" alt="Foto">
        <?php } ?>
    </div>
    <div class="col-sm-7" id="data">
        <table class="table siswa">
            <tbody>
                <tr>
                    <td width="150px">NPM</td>
                    <td width="5px">:</td>
                    <td>
                        <div class="col-sm-16">
                            <input type="text" name="npm" class="form-control" value="<?=isset($student['npm']) ? $student['npm'] : set_value('npm')?>" readonly>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="150px">Nama</td>
                    <td width="5px">:</td>
                    <td>
                        <div class="col-sm-16">
                            <input type="text" name="nama" class="form-control" value="<?=isset($student['nama']) ? $student['nama'] : set_value('nama')?>" readonly>
                        </div>  
                    </td>
                </tr>
                <tr>
                    <td width="150px">Program Studi</td>
                    <td width="5px">:</td>
                    <td>
                        <div class="col-sm-16">
                            <input type="text" name="prodi" class="form-control" value="<?=isset($student['prodi']) ? $student['prodi'] : set_value('prodi')?>" readonly>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="150px">Judul </td>
                    <td width="5px">:</td>
                    <td>
                        <div class="col-sm-16">
                            <textarea class="form-control" rows="4" name="judul" readonly> <?=isset($student['judul']) ? $student['judul'] : set_value('judul')?> </textarea>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="150px">Pembimbing 1</td>
                    <td width="5px">:</td>
                    <td>
                        <div class="col-sm-16">
                            <input type="text" name="pembimbing1" class="form-control" value="<?=isset($student['pembimbing1']) ? $student['pembimbing1'] : set_value('pembimbing1')?>" readonly>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="150px">Pembimbing 2</td>
                    <td width="5px">:</td>
                    <td>
                        <div class="col-sm-16">
                            <input type="text" name="pembimbing1" class="form-control" value="<?=isset($student['pembimbing2']) ? $student['pembimbing2'] : set_value('pembimbing2')?>" readonly>
                           
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="150px">Abstrak</td>
                    <td width="5px">:</td>
                    <td>
                        <div class="col-sm-16">
                            <textarea class="form-control" rows="20" name="abstrak_judul" readonly> <?=isset($student['abstrak_judul']) ? $student['abstrak_judul'] : set_value('abstrak_judul')?> </textarea>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td width="150px">Status</td>
                    <td width="5px">:</td>
                    <td>
                        <?php if ($student['s_status'] == "Selesai") { ?>
                            <span class="btn btn-success btn-sm kurang">Selesai</span>
                        <?php } else { ?>
                            <span class="btn btn-danger btn-sm belum">Belum Selesai</span>
                        <?php } ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-sm-2">
        <a class="btn btn-info pull-right mb hp" href="#" onclick="window.print()"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Cetak</a>
        <a href="<?=site_url('data_mahasiswa')?>" class="btn btn-primary pull-right mb hp"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Kembali</a>
    </div>
</div>

<div class="table-responsive">
    <h3><?=$attachment?></h3>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th width="10%">Daftar<br>Isi,Tabel,Gambar</th>
                <th width="8%">Abstrak</th>
                <th width="8%">Bab 1</th>
                <th width="8%">Bab 2</th>
                <th width="8%">Bab 3</th>
                <th width="8%">Bab 4</th>
                <th width="8%">Bab 5</th>
                <th width="8%">Daftar<br>Pustaka</th>
                <th width="8%">Lampiran</th>
                <th width="8%">Full Skripsi</th>
                <th width="9%">Source Code</th>
                <th width="10%">Compile Program</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><?=(!empty($student['daftarisi'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['abstrak'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['bab1'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['bab2'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['bab3'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['bab4'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['bab5'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['daftarpustaka'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['lampiran'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['fullskripsi'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['sourcecode'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['program'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
            </tr>
            <tr class="hp">
                <td>
                    <?php if (!empty($student['daftarisi'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/daftarisi/'.$student['daftarisi'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['abstrak'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/abstrak/'.$student['abstrak'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['bab1'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/bab1/'.$student['bab1'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['bab2'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/bab2/'.$student['bab2'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['bab3'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/bab3/'.$student['bab3'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['bab4'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/bab4/'.$student['bab4'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['bab5'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/bab5/'.$student['bab5'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['daftarpustaka'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/daftarpustaka/'.$student['daftarpustaka'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['lampiran'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/lampiran/'.$student['lampiran'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['fullskripsi'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/fullskripsi/'.$student['fullskripsi'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['sourcecode'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/sourcecode/'.$student['sourcecode'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['program'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/program/'.$student['program'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
            </tr>
        </tbody>
    </table>
</div>
