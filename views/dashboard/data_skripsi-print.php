<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!doctype html> 
<html lang="id">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" href="<?=site_url('assets/img/logo.png')?>">
        <meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1">
        <title><?=$title?></title>
        <?=link_tag('assets/css/bootstrap.css?ver=3.3.7')?>
        <?=link_tag('assets/css/style.css?ver=1.0.0')?>
    </head>
    <body onload="window.print()">
        <div class="container isi">
            <div class="row print">
                <div class="col-sm-3" id="foto">
                    <?php if (!empty($student['foto'])) { ?>
                        <img class="foto" src="<?=site_url('uploads/foto/'.$student['foto'])?>" alt="Foto">
                    <?php } else { ?>
                        <img class="foto" src="<?=site_url('assets/img/avatar.png')?>" alt="Foto">
                    <?php } ?>
                </div>
                <div class="col-sm-9" id="data">
                    <table class="table siswa">
                        <tbody>
                           <tr>
                                <td width="150px">NPM</td>
                                <td width="5px">:</td>
                                <td><?=$student['npm']?></td>
                            </tr>
                            <tr>
                                <td width="150px">Nama</td>
                                <td width="5px">:</td>
                                <td><?=$student['nama']?></td>
                            </tr>
                            <tr>
                                <td width="150px">Program Studi</td>
                                <td width="5px">:</td>
                                <td><?=$student['prodi']?></td>
                            </tr>
                            <tr>
                                <td width="150px">Judul </td>
                                <td width="5px">:</td>
                                <td><?=$student['judul']?></td>
                            </tr>
                            <tr>
                                <td width="150px">Pembimbing 1</td>
                                <td width="5px">:</td>
                                <td><?=$student['pembimbing1']?></td>
                            </tr>
                            <tr>
                                <td width="150px">Pembimbing 2</td>
                                <td width="5px">:</td>
                                <td><?=$student['pembimbing2']?></td>
                            </tr>
                            <tr>
                                <td width="150px">Abstrak</td>
                                <td width="5px">:</td>
                                <td><?=$student['abstrak_judul']?></td>
                            </tr>

                            <tr>
                                <td width="150px">Status</td>
                                <td width="5px">:</td>
                                <td>
                                    <?php if ($student['s_status'] == "Lengkap") { ?>
                                        <span class="btn btn-success btn-sm">Selesai</span>
                                    <?php } else if ($student['s_status'] == "Kurang") { ?>
                                        <span class="btn btn-warning btn-sm kurang">Selesai</span>
                                    <?php } else { ?>
                                        <span class="btn btn-danger btn-sm belum">Belum Selesai</span>
                                    <?php } ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
    <div class="tc">
    <h3><?=$attachment?></h3>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th width="10%">Daftar<br>Isi,Tabel,Gambar</th>
                <th width="8%">Abstrak</th>
                <th width="8%">Bab 1</th>
                <th width="8%">Bab 2</th>
                <th width="8%">Bab 3</th>
                <th width="8%">Bab 4</th>
                <th width="8%">Bab 5</th>
                <th width="8%">Daftar<br>Pustaka</th>
                <th width="8%">Lampiran</th>
                <th width="8%">Full Skripsi</th>
                <th width="9%">Source Code</th>
                <th width="10%">Compile Program</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><?=(!empty($student['daftarisi'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['abstrak'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['bab1'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['bab2'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['bab3'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['bab4'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['bab5'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['daftarpustaka'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['lampiran'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['fullskripsi'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['sourcecode'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['program'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
            </tr>
            <tr class="hp">
                <td>
                    <?php if (!empty($student['daftarisi'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/daftarisi/'.$student['daftarisi'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['abstrak'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/abstrak/'.$student['abstrak'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['bab1'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/bab1/'.$student['bab1'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['bab2'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/bab2/'.$student['bab2'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['bab3'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/bab3/'.$student['bab3'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['bab4'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/bab4/'.$student['bab4'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['bab5'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/bab5/'.$student['bab5'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['daftarpustaka'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/daftarpustaka/'.$student['daftarpustaka'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['lampiran'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/lampiran/'.$student['lampiran'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['fullskripsi'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/fullskripsi/'.$student['fullskripsi'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['sourcecode'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/sourcecode/'.$student['sourcecode'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['program'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/program/'.$student['program'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
            </tr>
        </tbody>
    </table>
</div>
        </div>
    </body>
</html>
