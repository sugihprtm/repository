<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1>Selamat Datang <small><?=$this->session->userdata['u_fname']?></small></h1>
<h3>Berikut Adalah Statistik Data Tugas Akhir Fasilkom Unsika </h3>

<br>
<div class="row">
    

    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Jumlah Mahasiswa</h3>
            </div>
            <div class="panel-body row">
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-body red">
                            <h1 class="ttl"><?=$total?></h1>
                            <h3>Mahasiswa</h3>
                        </div>
                        <div class="panel-footer red">Total Mahasiswa</div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-body blue">
                            <h1 class="ttl"><?=$persenms?>%</h1>
                            <h3><?=$totalms?> Mahasiswa</h3>
                        </div>
                        <div class="panel-footer blue">Mahasiswa Selesai Skripsi</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Jumlah Tugas Akhir</h3>
            </div>
            <div class="panel-body row">
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-body red">
                            <h1 class="ttl"><?=$persenb?>%</h1>
                            <h3><?=$totalb?> Mahasiswa</h3>
                        </div>
                        <div class="panel-footer red">Belum Publis</div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-body green">
                            <h1 class="ttl"><?=$persenl?>%</h1>
                            <h3><?=$totall?> Mahasiswa</h3>
                        </div>
                        <div class="panel-footer green">Publis</div>
                    </div>
                </div>
            </div>
        </div> 
    </div>
    
</div>
