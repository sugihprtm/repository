<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row print">
    <div class="col-sm-3" id="foto">
        <?php if (!empty($student['foto'])) { ?>
            <img class="foto" src="<?=site_url('uploads/foto/'.$student['foto'])?>" alt="Foto">
        <?php } else { ?>
            <img class="foto" src="<?=site_url('assets/img/avatar.png')?>" alt="Foto">
        <?php } ?>
    </div>
    <div class="col-sm-7" id="data">
        <table class="table siswa">
            <tbody>
                <tr>
                    <td width="150px">NPM</td>
                    <td width="5px">:</td>
                    <td><?= $this->session->userdata['u_name']; ?></td>
                </tr>
                <tr>
                    <td width="150px">Nama</td>
                    <td width="5px">:</td>
                    <td><?=$student['nama']?></td>
                </tr>
                 <tr>
                    <td width="150px">Tempat Lahir</td>
                    <td width="5px">:</td>
                    <td><?=$student['pob']?></td>
                </tr>
                <tr>
                    <td width="150px">Tanggal Lahir</td>
                    <td width="5px">:</td>
                    <td><?=$student['dob']?></td>
                </tr>
                <tr>
                    <td width="150px">Jenis Kelamin</td>
                    <td width="5px">:</td>
                    <td><?=$student['jenis_kelamin']?></td>
                </tr>
                <tr>
                    <td width="150px">Alamat</td>
                    <td width="5px">:</td>
                    <td><?=$student['alamat']?></td>
                </tr>
                <tr>
                    <td width="150px">Program Studi</td>
                    <td width="5px">:</td>
                    <td><?=$student['prodi']?></td>
                </tr>
                <tr>
                    <td width="150px">No Handphone </td>
                    <td width="5px">:</td>
                    <td><?=$student['no_hp']?></td>
                </tr>
                <tr>
                    <td width="150px">Email</td>
                    <td width="5px">:</td>
                    <td><?=$student['email']?></td>
                </tr>

                <tr>
                    <td width="150px">Status</td>
                    <td width="5px">:</td>
                    <td>
                        <?php if ($student['s_status'] == "Selesai") { ?>
                            <span class="btn btn-success btn-sm">Selesai</span>
                        <?php } else { ?>
                            <span class="btn btn-danger btn-sm belum">Belum Selesai</span>
                        <?php } ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-sm-2">
        <a class="btn btn-info pull-right mb hp" href="#" onclick="window.print()"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Cetak</a>
        <?php if ($student['s_is_active'] == "Aktif") { ?>
            <a href="<?=site_url('profil/edit/'.$this->session->userdata['u_name'])?>" class="btn btn-primary pull-right mb hp"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Edit Data</a>
        <?php } ?>
    </div>
</div>
