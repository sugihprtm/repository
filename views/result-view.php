<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row print">
    <div class="col-sm-3" id="foto">
        <?php if (!empty($student['foto'])) { ?>
            <img class="foto" src="<?=site_url('uploads/foto/'.$student['foto'])?>" alt="Foto">
        <?php } else { ?>
            <img class="foto" src="<?=site_url('assets/img/avatar.png')?>" alt="Foto">
        <?php } ?>
    </div>
    <div class="col-sm-7" id="data">
        <table class="table siswa">
            <tbody>
               <tr>
                    <td width="160px">NPM</td>
                    <td width="5px">:</td>
                    <td width="500px"><?=$student['npm']?></td>
                </tr>
                <tr>
                    <td width="160px">Nama</td>
                    <td width="5px">:</td>
                    <td width="100px"><?=$student['nama']?></td>
                </tr>
                <tr>
                    <td width="160px">Tempat, Tanggal Lahir</td>
                    <td width="">:</td>
                    <td width="100px"><?=$student['pob']?>, <?=$student['dob']?></td>
                </tr>
                <tr>
                    <td width="160px">Program Studi</td>
                    <td width="5px">:</td>
                    <td width="100px"><?=$student['prodi']?></td>
                </tr>
                <tr>
                    <td width="160px">Judul</td>
                    <td width="5px">:</td>
                    <td width="100px"><div class="col-sm-16">
                            <textarea class="form-control" rows="3" name="judul" readonly> <?=isset($student['judul']) ? $student['judul'] : set_value('judul')?> </textarea>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="160px">Pembimbing 1</td>
                    <td width="5px">:</td>
                    <td width="100px"><?=$student['pembimbing1']?></td>
                </tr>
                <tr>
                    <td width="160px">Pembimbing 2</td>
                    <td width="5px">:</td>
                    <td width="100px"><?=$student['pembimbing2']?></td>
                </tr>
                <tr>
                    <td width="160px">Abstrak</td>
                    <td width="5px">:</td>
                    <td width="100px"><div class="col-sm-16">
                            <textarea class="form-control" rows="20" name="abstrak_judul" readonly> <?=isset($student['abstrak_judul']) ? $student['abstrak_judul'] : set_value('abstrak_judul')?> </textarea>
                        </div></td>
                </tr>
                <tr>
                    <td width="160px">Status</td>
                    <td width="5px">:</td>
                    <td width="100px">
                        <?php if ($student['s_status'] == "Lengkap") { ?>
                            <span class="btn btn-success btn-sm">Publis</span>
                        <?php } else { ?>
                            <span class="btn btn-danger btn-sm belum">Belum Publis</span>
                        <?php } ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-sm-2">
        <a class="btn btn-success pull-right mb hp" href="<?=site_url()?>"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Kembali</a>
        <a class="btn btn-info pull-right mb hp" href="#" onclick="window.print()"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Cetak</a>
    </div>
</div>
<div class="tc">
    <h3><?=$attachment?></h3>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th width="14.285%">Abstrak</th>
                <th width="14.285%">Bab 1</th>
                <th width="14.285%">Bab 2</th>
                <th width="14.285%">Bab 4</th>
                <th width="14.285%">Bab 4</th>
                <th width="14.285%">Bab 5</th>
                <th width="14.285%">Lampiran</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><?=(!empty($student['abstrak'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['bab1'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['bab2'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['bab3'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['bab4'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['bab5'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
                <td><?=(!empty($student['lampiran'])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "&minus;";?></td>
            </tr>
            <tr class="hp">
                <td>
                    <?php if (!empty($student['abstrak'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/kk/'.$student['abstrak'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['bab1'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/ktpa/'.$student['bab1'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['bab2'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/ktpi/'.$student['bab2'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['bab3'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/kips/'.$student['bab3'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['bab4'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/sktm/'.$student['bab4'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['bab5'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/ijazah/'.$student['bab5'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
                <td>
                    <?php if (!empty($student['lampiran'])) { ?>
                        <a class="btn btn-default btn-xs hp" href="<?=site_url('uploads/skhun/'.$student['lampiran'])?>" target="_blank">Lihat</a>
                    <?php } ?>
                </td>
            </tr>
        </tbody>
    </table>
</div>
