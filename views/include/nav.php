<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php
    if ($this->session->flashdata('alert')) {
        echo $this->session->flashdata('alert');
} ?>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?=site_url('dashboard')?>"><img class="brand-logo" src="<?=site_url('assets/img/fasilkom.png')?>" alt="">SISTEM INFORMASI TUGAS AKHIR</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="<?=site_url('dashboard')?>">Beranda</a></li>

                <?php if ($this->session->userdata['u_level'] == "Mahasiswa")  { ?>
                    <li><a href="<?=site_url('cari_skripsi')?>">Cari Tugas Akhir</a></li>
                <?php } ?>

                <?php if ($this->session->userdata['u_level'] == "Mahasiswa")  { ?>
                    <li><a href="<?=site_url('profil')?>">Profil</a></li>
                <?php } ?>

                <?php if ($this->session->userdata['u_status'] == "Selesai")  { ?>
                    <li><a href="<?=site_url('upload_skripsi')?>">Tugas Akhir</a></li>
                <?php } ?>

                <?php if ($this->session->userdata['u_level'] == "Administrator")  { ?>
                    <li><a href="<?=site_url('data_mahasiswa')?>">Data Mahasiswa</a></li>
                <?php } ?>

                <?php if ($this->session->userdata['u_level'] == "Administrator")  { ?>
                    <li><a href="<?=site_url('data_skripsi')?>">Data Skripsi</a></li>
                <?php } ?>

                <?php if ($this->session->userdata['u_level'] == "Administrator")  { ?>
                    <li><a href="<?=site_url('prodi')?>">Data Prodi</a></li>
                <?php } ?>

                 <?php if ($this->session->userdata['u_level'] == "Administrator")  { ?>
                    <li><a href="<?=site_url('dosen')?>">Data Dosen</a></li>
                <?php } ?>

                <?php if ($this->session->userdata['u_level'] == "Dosen")  { ?>
                    <li><a href="<?=site_url('data_mahasiswa')?>">Data Mahasiswa</a></li>
                <?php } ?>

                <?php if ($this->session->userdata['u_level'] == "Dosen")  { ?>
                    <li><a href="<?=site_url('data_skripsi')?>">Data Skripsi</a></li>
                <?php } ?>

                <?php if ($this->session->userdata['u_level'] == "Administrator")  { ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Data Terhapus <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?=site_url('data_mahasiswa/deleted')?>">Data Mahasiswa Terhapus</a></li>
                            <?php if ($this->session->userdata['u_level'] == "Administrator")  { ?>
                                <li role="separator" class="divider"></li>
                                <li><a href="<?=site_url('prodi/deleted')?>">Data Prodi Terhapus</a></li>
                            <?php } ?>
                            <?php if ($this->session->userdata['u_level'] == "Administrator")  { ?>
                                <li role="separator" class="divider"></li>
                                <li><a href="<?=site_url('dosen/deleted')?>">Data Dosen Terhapus</a></li>
                            <?php } ?>
                        </ul>
                    </li>
                <?php } ?>

                <?php if ($this->session->userdata['u_level'] == "Administrator")  { ?>
                    <li><a href="<?=site_url('user')?>">Data User</a></li>
                <?php } ?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <?=$this->session->userdata['u_name']?> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?=site_url('user/profile')?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Ubah Password</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?=site_url('logout')?>"><span class="glyphicon glyphicon-off" aria-hidden="true"></span> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
