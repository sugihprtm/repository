<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<footer class="footer"> 
    <div class="container">
        <p>&copy; <a href="http://cs.unsika.ac.id" target="_blank">Fakultas Ilmu Komputer, Universitas Singaperbangsa Karawang - 2018</a></p>
    </div>
</footer>
<script src="<?=base_url('assets/js/jquery.min.js?ver=3.2.0')?>"></script>
<script src="<?=base_url('assets/js/bootstrap.min.js?ver=3.3.7')?>"></script>
<script src="<?=base_url('assets/js/jquery.dataTables.min.js?ver=1.10.13')?>"></script>
<script src="<?=base_url('assets/js/dataTables.bootstrap.min.js?ver=1.10.13')?>"></script>

<script type="text/javascript">
    $(document).ready(function() {

        // Notification alert
        $("#notif").delay(350).slideDown('slow');
        $("#notif").alert().delay(3000).slideUp('slow');

        // Live search
        $("#search").keyup(function() {
            var str =  $("#search").val();
            if (str == "") {
                $("#hint" ).html("<p class='help-block'>Masukkan Judul / Kata Kunci dan hasil akan otomatis ditampilkan disini.<br></p>");
            } else {
                $.get("<?=site_url()?>home/result?keyword="+str, function(data) {
                    $("#hint").html(data);
                });
            }
        });

        // Live search
        $("#search_skripsi").keyup(function() {
            var str =  $("#search_skripsi").val();
            if (str == "") {
                $("#hint" ).html("<p class='help-block'>Masukkan Judul / Kata Kunci dan hasil akan otomatis ditampilkan disini.<br></small></p>");
            } else {
                $.get("<?=site_url()?>lihat_skripsi/result?keyword="+str, function(data) {
                    $("#hint").html(data);
                });
            }
        });

        // Report
        $("#tampilkan").click(function() {
            var action = $("#report").attr('action');
            var report = {
                s_grade: $("#s_grade").val(),
                m_id: $("#m_id").val(),
                s_status: $("#s_status").val()
            };

            $.ajax({
                type: "GET",
                url: action,
                data: report,
                beforeSend: function() {
                    $('#tampil').html('Sedang memuat.....');
                    $('.btn').addClass('disabled');
                },
                success: function(result) {
                    $("#result").html(result);
                    $('#tampil').html('Tampilkan');
                    $('.btn').removeClass('disabled');
                }
            });
            return false;
        });

        // Show hide password
        $('#pass').on('click', function() {
            if ($('#password').attr('pass-shown') == 'false') {
                $('#password').removeAttr('type');
                $('#password').attr('type', 'text');
                $('#password').removeAttr('pass-shown');
                $('#password').attr('pass-shown', 'true');
                $('#pass').html('<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>');
            } else {
                $('#password').removeAttr('type');
                $('#password').attr('type', 'password');
                $('#password').removeAttr('pass-shown');
                $('#password').attr('pass-shown', 'false');
                $('#pass').html('<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>');
            }
        });

        // Ajax login
        $("#btn-login").click(function() {
            var formAction = $("#login").attr('action');
            var datalogin = {
                u_name: $("#username").val(),
                u_pass: $("#password").val(),
                csrf_token: $.cookie('csrf_cookie')
            };

            $.ajax({
                type: "POST",
                url: formAction,
                data: datalogin,
                beforeSend: function() {
                    $('#status').html('Sedang memproses.....');
                    $('.btn-block').addClass('disabled');
                },
                success: function(data) {
                    if (data == 1) {
                        $("#success").slideDown('slow');
                        $("#success").alert().delay(6000).slideUp('slow');
                        setTimeout(function() {
                            window.location = '<?=site_url('dashboard')?>';
                        }, 2000);
                    } else {
                        $('#status').html('Login');
                        $('.btn-block').removeClass('disabled');
                        $("#failed").slideDown('slow');
                        $("#failed").alert().delay(3000).slideUp('slow');
                        return false;
                    }
                }
            });
            return false;
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        // View data student
        var student = $('#student').DataTable({
            "processing": true,
            "language": {
                "processing": "<span class='glyphicon glyphicon-refresh' aria-hidden='true'></span> Sedang memuat....."
            },
            "serverSide": true,
            "ajax": "<?=site_url('profil/get_data')?>",
            "columns": [
                {
                    "data": null,
                    "orderable": true
                },
                {"data": "npm"},
                {"data": "nama"},
                {"data": "pob"},
                {"data": "dob"},
                {"data": "jenis_kelamin"},
                {"data": "alamat"},
                {"data": "prodi"},
                {"data": "no_hp"},
                {"data": "email"},
                {"data": "tindakan"}
            ],
            "order": [[1, 'asc']],
            "rowCallback": function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });

        // View data mahasiswa
        var mahasiswa = $('#mahasiswa').DataTable({
            "processing": true,
            "language": {
                "processing": "<span class='glyphicon glyphicon-refresh' aria-hidden='true'></span> Sedang memuat....."
            },
            "serverSide": true,
            "ajax": "<?=site_url('data_mahasiswa/get_data')?>",
            "columns": [
                {
                    "data": null,
                    "orderable": true
                },
                {"data": "npm"},
                {"data": "nama"},
                {"data": "pob"},
                {"data": "dob"},
                {"data": "jenis_kelamin"},
                {"data": "alamat"},
                {"data": "prodi"},
                {"data": "no_hp"},
                {"data": "email"},
                {"data": "tindakan"}
            ],
            "order": [[1, 'asc']],
            "rowCallback": function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });

        // View data major
        var prodi = $('#prodi').DataTable({
            "processing": true,
            "language": {
                "processing": "<span class='glyphicon glyphicon-refresh' aria-hidden='true'></span> Sedang memuat....."
            },
            "serverSide": true,
            "ajax": "<?=site_url('prodi/get_data')?>",
            "columns": [
                {
                    "data": null,
                    "orderable": true
                },
                {"data": "prodi"},
                {"data": "m_id"},
                {"data": "m_created_at"},
                {"data": "m_updated_at"},
                {"data": "tindakan"}
            ],
            "order": [[1, 'asc']],
            "rowCallback": function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
        
        // View data major_deleted
        var prodi_deleted = $('#prodi-deleted').DataTable({
            "processing": true,
            "language": {
                "processing": "<span class='glyphicon glyphicon-refresh' aria-hidden='true'></span> Sedang memuat....."
            },
            "serverSide": true,
            "ajax": "<?=site_url('prodi/get_deleted')?>",
            "columns": [
                {
                    "data": null,
                    "orderable": true
                },
                {"data": "prodi"},
                {"data": "m_id"},
                {"data": "m_created_at"},
                {"data": "m_deleted_at"},
                {"data": "tindakan"}
            ],
            "order": [[1, 'asc']],
            "rowCallback": function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });

        // View data dosen
        var dosen = $('#dosen').DataTable({
            "processing": true,
            "language": {
                "processing": "<span class='glyphicon glyphicon-refresh' aria-hidden='true'></span> Sedang memuat....."
            },
            "serverSide": true,
            "ajax": "<?=site_url('dosen/get_data')?>",
            "columns": [
                {
                    "data": null,
                    "orderable": true
                },
                {"data": "nidn"},
                {"data": "nama"},
                {"data": "d_created_at"},
                {"data": "d_updated_at"},
                {"data": "tindakan"}
            ],
            "order": [[1, 'asc']],
            "rowCallback": function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
        
        // View data dosen_deleted
        var dosen_deleted = $('#dosen-deleted').DataTable({
            "processing": true,
            "language": {
                "processing": "<span class='glyphicon glyphicon-refresh' aria-hidden='true'></span> Sedang memuat....."
            },
            "serverSide": true,
            "ajax": "<?=site_url('dosen/get_deleted')?>",
            "columns": [
                {
                    "data": null,
                    "orderable": true
                },
                {"data": "nidn"},
                {"data": "nama"},
                {"data": "d_created_at"},
                {"data": "d_deleted_at"},
                {"data": "tindakan"}
            ],
            "order": [[1, 'asc']],
            "rowCallback": function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });

        // View data user
        var user = $('#user').DataTable({
            "processing": true,
            "language": {
                "processing": "<span class='glyphicon glyphicon-refresh' aria-hidden='true'></span> Sedang memuat....."
            },
            "serverSide": true,
            "ajax": "<?=site_url('user/get_data')?>",
            "columns": [
                {
                    "data": null,
                    "orderable": true
                },
                {"data": "u_name"},
                {"data": "u_fname"},
                {"data": "u_level"},
                {"data": "u_is_active"},
                {"data": "u_last_logged_in"},
                {"data": "tindakan"}
            ],
            "order": [[1, 'asc']],
            "rowCallback": function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });

        // View data skripsi
        var skripsi = $('#skripsi').DataTable({
            "processing": true,
            "language": {
                "processing": "<span class='glyphicon glyphicon-refresh' aria-hidden='true'></span> Sedang memuat....."
            },
            "serverSide": true,
            "ajax": "<?=site_url('data_skripsi/get_data')?>",
            "columns": [
                {
                    "data": null,
                    "orderable": true
                },
                {"data": "npm"},
                {"data": "nama"},
                {"data": "prodi"},
                {"data": "judul"},
                {"data": "pembimbing1"},
                {"data": "pembimbing2"},
                {"data": "tindakan"}
            ],
            "order": [[1, 'asc']],
            "rowCallback": function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });

        // View cari data skripsi
        var cari_skripsi = $('#cari_skripsi').DataTable({
            "processing": true,
            "language": {
                "processing": "<span class='glyphicon glyphicon-refresh' aria-hidden='true'></span> Sedang memuat....."
            },
            "serverSide": true,
            "ajax": "<?=site_url('cari_skripsi/get_data')?>",
            "columns": [
                {
                    "data": null,
                    "orderable": true
                },
                {"data": "npm"},
                {"data": "nama"},
                {"data": "prodi"},
                {"data": "judul"},
                {"data": "pembimbing1"},
                {"data": "pembimbing2"},
                {"data": "tindakan"}
            ],
            "order": [[1, 'asc']],
            "rowCallback": function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });

        // View data student_deleted
        var mahasiswa_deleted = $('#mahasiswa-deleted').DataTable({
            "processing": true,
            "language": {
                "processing": "<span class='glyphicon glyphicon-refresh' aria-hidden='true'></span> Sedang memuat....."
            },
            "serverSide": true,
            "ajax": "<?=site_url('data_mahasiswa/get_deleted')?>",
            "columns": [
                {
                    "data": null,
                    "orderable": true
                },
                {"data": "npm"},
                {"data": "nama"},
                {"data": "pob"},
                {"data": "dob"},
                {"data": "jenis_kelamin"},
                {"data": "alamat"},
                {"data": "prodi"},
                {"data": "no_hp"},
                {"data": "email"},
                {"data": "tindakan"}
            ],
            "order": [[1, 'asc']],
            "rowCallback": function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });

        // View data student_archived
        var student_archived = $('#student-archived').DataTable({
            "processing": true,
            "language": {
                "processing": "<span class='glyphicon glyphicon-refresh' aria-hidden='true'></span> Sedang memuat....."
            },
            "serverSide": true,
            "ajax": "<?=site_url('student/get_archived')?>",
            "columns": [
                {
                    "data": null,
                    "orderable": true
                },
                {"data": "s_nisn"},
                {"data": "s_name"},
                {"data": "s_gender"},
                {"data": "s_grade"},
                {"data": "m_id"},
                {"data": "s_yi"},
                {"data": "s_yo"},
                {"data": "s_is_active"},
                {"data": "tindakan"}
            ],
            "order": [[1, 'asc']],
            "rowCallback": function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });

        
    });
</script>
