-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 11 Okt 2018 pada 13.21
-- Versi Server: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tugas_akhir_fasilkom`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('8tkgaos200te25kfd38cfeuk720jbdkt', '::1', 1539256595, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533393235363539353b);

-- --------------------------------------------------------

--
-- Struktur dari tabel `dosen`
--

CREATE TABLE `dosen` (
  `nidn` varchar(12) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `d_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `d_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `d_deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `d_restored_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `d_created_by` varchar(20) NOT NULL,
  `d_updated_by` varchar(20) NOT NULL,
  `d_deleted_by` varchar(20) NOT NULL,
  `d_restored_by` varchar(20) NOT NULL,
  `d_is_deleted` enum('TRUE','FALSE') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dosen`
--

INSERT INTO `dosen` (`nidn`, `nama`, `d_created_at`, `d_updated_at`, `d_deleted_at`, `d_restored_at`, `d_created_by`, `d_updated_by`, `d_deleted_by`, `d_restored_by`, `d_is_deleted`) VALUES
('0009028307', 'Nina Sulistyowati, S.T., M.Kom.', '2018-09-17 06:29:32', '2018-09-17 06:29:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'septian', '', '', '', 'FALSE'),
('0402047903', 'Ade Andri Hendriadi, S.Si., M.Kom.', '2018-09-17 06:28:21', '2018-09-17 06:28:21', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'septian', '', '', '', 'FALSE');

-- --------------------------------------------------------

--
-- Struktur dari tabel `prodi`
--

CREATE TABLE `prodi` (
  `m_id` varchar(3) NOT NULL,
  `prodi` varchar(50) DEFAULT NULL,
  `m_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `m_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `m_deleted_at` timestamp NULL DEFAULT NULL,
  `m_restored_at` timestamp NULL DEFAULT NULL,
  `m_created_by` varchar(20) DEFAULT NULL,
  `m_updated_by` varchar(20) DEFAULT NULL,
  `m_deleted_by` varchar(20) DEFAULT NULL,
  `m_restored_by` varchar(20) DEFAULT NULL,
  `m_is_deleted` enum('TRUE','FALSE') DEFAULT 'FALSE'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `prodi`
--

INSERT INTO `prodi` (`m_id`, `prodi`, `m_created_at`, `m_updated_at`, `m_deleted_at`, `m_restored_at`, `m_created_by`, `m_updated_by`, `m_deleted_by`, `m_restored_by`, `m_is_deleted`) VALUES
('MM', 'Multimedia', '2017-06-26 22:05:28', '2018-07-24 06:16:59', '2018-07-24 01:16:59', '2018-07-24 01:16:44', 'xB3gG', NULL, 'septi', 'septi', 'TRUE'),
('SI', 'Sistem Informasi', '2018-07-19 03:33:26', '2018-09-29 03:36:39', NULL, NULL, 'MtjhU', 'admin 1', NULL, NULL, 'FALSE'),
('SK', 'Sistem Komputer', '2018-07-19 06:30:06', '2018-07-19 06:30:06', NULL, NULL, 'MtjhU', NULL, NULL, NULL, 'FALSE'),
('TI', 'Teknik Informatika', '2018-07-19 03:39:34', '2018-07-18 22:50:33', NULL, NULL, 'MtjhU', 'MtjhU', NULL, NULL, 'FALSE'),
('TK', 'Teknik Komputer', '2018-07-19 04:18:53', '2018-07-19 04:18:53', NULL, NULL, 'MtjhU', NULL, NULL, NULL, 'FALSE');

-- --------------------------------------------------------

--
-- Struktur dari tabel `students`
--

CREATE TABLE `students` (
  `npm` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `pob` varchar(20) NOT NULL,
  `dob` date NOT NULL,
  `jenis_kelamin` enum('Laki-laki','Perempuan') NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `prodi` varchar(50) NOT NULL,
  `no_hp` varchar(13) NOT NULL,
  `email` varchar(50) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `judul` varchar(150) NOT NULL,
  `abstrak_judul` text NOT NULL,
  `pembimbing1` varchar(50) NOT NULL,
  `pembimbing2` varchar(50) NOT NULL,
  `daftarisi` varchar(50) DEFAULT NULL,
  `abstrak` varchar(50) DEFAULT NULL,
  `bab1` varchar(50) DEFAULT NULL,
  `bab2` varchar(50) DEFAULT NULL,
  `bab3` varchar(50) DEFAULT NULL,
  `bab4` varchar(50) DEFAULT NULL,
  `bab5` varchar(50) DEFAULT NULL,
  `daftarpustaka` varchar(50) DEFAULT NULL,
  `lampiran` varchar(50) DEFAULT NULL,
  `fullskripsi` varchar(50) DEFAULT NULL,
  `sourcecode` varchar(50) DEFAULT NULL,
  `program` varchar(50) DEFAULT NULL,
  `s_publis` varchar(20) DEFAULT NULL,
  `s_status` varchar(12) DEFAULT NULL,
  `s_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `s_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `s_deleted_at` timestamp NULL DEFAULT NULL,
  `s_restored_at` timestamp NULL DEFAULT NULL,
  `s_created_by` varchar(20) DEFAULT NULL,
  `s_updated_by` varchar(20) DEFAULT NULL,
  `s_deleted_by` varchar(20) DEFAULT NULL,
  `s_restored_by` varchar(20) DEFAULT NULL,
  `s_is_deleted` enum('TRUE','FALSE') DEFAULT 'FALSE',
  `s_is_active` enum('Aktif','Tidak Aktif') NOT NULL DEFAULT 'Aktif'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `students`
--

INSERT INTO `students` (`npm`, `nama`, `pob`, `dob`, `jenis_kelamin`, `alamat`, `prodi`, `no_hp`, `email`, `foto`, `judul`, `abstrak_judul`, `pembimbing1`, `pembimbing2`, `daftarisi`, `abstrak`, `bab1`, `bab2`, `bab3`, `bab4`, `bab5`, `daftarpustaka`, `lampiran`, `fullskripsi`, `sourcecode`, `program`, `s_publis`, `s_status`, `s_created_at`, `s_updated_at`, `s_deleted_at`, `s_restored_at`, `s_created_by`, `s_updated_by`, `s_deleted_by`, `s_restored_by`, `s_is_deleted`, `s_is_active`) VALUES
('1510631170003', '', '', '0000-00-00', 'Laki-laki', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum', 'Belum', '2018-10-09 06:15:46', '2018-10-11 11:12:52', NULL, NULL, NULL, 'septian', NULL, NULL, 'FALSE', 'Aktif'),
('1510631170034', '', '', '0000-00-00', 'Laki-laki', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum', '2018-10-11 11:15:20', '2018-10-11 11:15:20', NULL, NULL, NULL, NULL, NULL, NULL, 'FALSE', 'Aktif'),
('1510631170133', 'Santi Nur Aini', 'Bandung', '1997-02-06', 'Perempuan', 'Perum Singaperbangsa, Blok A7 No. 1', 'Teknik Informatika', '081298776752', 'santi.nur15133@gmail.com', '1510631170133-foto.png', 'Data mining dengan Algoritma C4.5', 'Data mining adalah proses untuk mencari pola unik pada data yang sangat besar', 'Ade Andri Hendriadi, S.Si., M.Kom.', 'Nina Sulistyowati, S.T., M.Kom.', '1510631170133-daftarisi.pdf', '1510631170133-abstrak.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 'Publis', 'Selesai', '2018-07-18 07:59:29', '2018-10-11 05:51:27', NULL, NULL, 'nYroL', '1510631170133', NULL, NULL, 'FALSE', 'Aktif'),
('1510631170135', 'Septian Nurillah', 'Bekasi', '1996-09-20', 'Laki-laki', 'Jalan Mangga 1 RT 002/008 No. 64 Kecamatan Bekasi Selatan', 'Teknik Informatika', '086699996627', 'septian15135@gmail.com', '1510631170135-foto.png', 'Sistem Informasi Sekolah', 'SMAN 1 Karawang adalah salah satu SMA Negeri di Kabupatern Karawang. SMA ini terletak di pusat kota.', 'Ade Andri Hendriadi, S.Si., M.Kom.', 'Nina Sulistyowati, S.T., M.Kom.', '1510631170135-daftarisi.pdf', '1510631170135-abstrak.pdf', '1510631170135-bab1.pdf', '1510631170135-bab2.pdf', '1510631170135-bab3.pdf', '1510631170135-bab4.pdf', '1510631170135-bab5.pdf', '1510631170135-daftarpustaka.pdf', '1510631170135-lampiran.pdf', '1510631170135-fullskripsi.pdf', '1510631170135-sourcecode.rar', '1510631170135-program.rar', '', 'Selesai', '2018-09-17 05:06:53', '2018-10-11 03:49:07', NULL, NULL, NULL, 'septian', NULL, NULL, 'FALSE', 'Aktif');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `u_name` varchar(20) DEFAULT NULL,
  `u_pass` varchar(255) DEFAULT NULL,
  `u_fname` varchar(50) DEFAULT NULL,
  `u_level` enum('Administrator','Mahasiswa','Dosen') NOT NULL,
  `u_status` varchar(20) NOT NULL,
  `u_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `u_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `u_created_by` varchar(25) DEFAULT NULL,
  `u_updated_by` varchar(25) DEFAULT NULL,
  `u_password_updated_at` timestamp NULL DEFAULT NULL,
  `u_last_logged_in` timestamp NULL DEFAULT NULL,
  `u_ip_address` varchar(50) DEFAULT NULL,
  `u_is_active` enum('Aktif','Tidak Aktif') NOT NULL DEFAULT 'Aktif'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`u_name`, `u_pass`, `u_fname`, `u_level`, `u_status`, `u_created_at`, `u_updated_at`, `u_created_by`, `u_updated_by`, `u_password_updated_at`, `u_last_logged_in`, `u_ip_address`, `u_is_active`) VALUES
('septian', '$2y$05$2r04glUjIMc92Ea1DpF1SO1zwWRtFJ82ypS1bL/F0nMUO48chhR4m', 'Septian Ilham Mulfahmi', 'Administrator', '', '2018-07-16 09:43:48', '2018-10-11 11:13:10', 'xB3gG', 'RnpCB', NULL, '2018-10-11 06:13:10', '::1', 'Aktif'),
('1510631170133', '$2y$05$FHw9tzOdG4RNzh.ppZQA3uHj7UDch2CkTtAaR5VIB4YUicIHdyzPe', 'Santi Nur Aini', 'Mahasiswa', 'Selesai', '2018-07-23 06:06:35', '2018-10-11 10:58:00', 'septi', 'septi', '2018-09-20 02:13:33', '2018-10-11 05:58:00', '::1', 'Aktif'),
('1510631170135', '$2y$05$kYoOUsrcuZsqGgM2Im6OcuW2b1tNfqc04YrEaGRDkGN7dRa8xW7hm', 'Septian Nurillah', 'Mahasiswa', 'Selesai', '2018-09-17 05:06:53', '2018-10-11 10:17:31', 'septi', NULL, NULL, '2018-10-11 05:17:31', '::1', 'Aktif'),
('admin 1', '$2y$05$l6DaPCaeZyWh3gA7qEWPkeZftLn0TJ.IkpZ4tthsqQXHNSrkQeAUu', 'Supriono', 'Administrator', 'Belum', '2018-09-29 08:14:22', '2018-10-02 05:24:02', 'septi', NULL, NULL, '2018-10-02 00:24:02', '::1', 'Aktif'),
('admin 2', '$2y$05$z80jWrJsRdcC01I0Ce.QquP0cen9y0PpQGozxgmHBfdLVZyyQEtWC', 'admin 2', 'Administrator', 'Belum', '2018-10-01 23:11:46', '2018-10-01 23:11:46', 'septi', NULL, NULL, NULL, NULL, 'Aktif'),
('dosen', '$2y$05$zMTsvOETTTQAxbdJBjC3c.2vByQ.GX.227OOcx4k.MLh71rrHR2Zm', 'Dosen', 'Dosen', 'Belum', '2018-10-02 05:24:35', '2018-10-02 05:45:37', 'admin', NULL, NULL, '2018-10-02 00:45:36', '::1', 'Aktif'),
('1510631170003', '$2y$05$HieIYQPmzyfPgYDiC4G3LOWw.X.1o3ixw6uiC0jkMEQe2cJpMdnrS', 'Ahmad', 'Mahasiswa', 'Selesai', '2018-10-09 06:15:46', '2018-10-11 09:02:13', 'septi', NULL, NULL, NULL, NULL, 'Aktif'),
('1510631170034', '$2y$05$FZFnc/Qj3.08f3JcjL6ureRx6KpKewt0wPOU0GNdFZmNQEDlbrloe', 'Deni', 'Mahasiswa', 'Belum', '2018-10-11 11:15:20', '2018-10-11 11:15:42', 'septian', NULL, NULL, '2018-10-11 06:15:42', '::1', 'Aktif');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`nidn`);

--
-- Indexes for table `prodi`
--
ALTER TABLE `prodi`
  ADD PRIMARY KEY (`m_id`),
  ADD UNIQUE KEY `mname` (`prodi`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD UNIQUE KEY `snisn` (`npm`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD UNIQUE KEY `uname` (`u_name`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
